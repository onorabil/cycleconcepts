import numpy as np
from neural_wrappers.callbacks import Callback

class MedianCallback(Callback):
	def __init__(self, cfg, reader):
		super().__init__()
		self.res = np.zeros((0, 1), dtype=np.float32)
		self.cfg = cfg
		self.reader = reader

	def onIterationEnd(self, results, labels, **kwargs):
		key = list(results.keys())[-1]
		results = results[key][0]
		labelKey = self.cfg.split("2")[-1]
		# TODO for other skip values
		if labelKey == "opticalflow":
			labelKey = "optical_flow(t+1)"
		# TODO: quaternions and such
		elif labelKey == "pose":
			labelKey = "pose_6dof"

		labels = labels[labelKey]
		res = (results - labels)**2

		if labelKey == "pose_6dof":
			res = res.mean(axis=-1)
		else:
			res = res.mean(axis=-1).mean(axis=-1).mean(axis=-1)

		res = np.expand_dims(res, axis=-1)
		self.res = np.concatenate([self.res, res], axis=0)

	def onEpochEnd(self, **kwargs):
		print("%s. Mean: %2.7f. Median: %2.7f" % (self.cfg, self.res.mean(), np.median(self.res)))