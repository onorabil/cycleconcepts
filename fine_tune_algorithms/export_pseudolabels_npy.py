import numpy as np
from tqdm import tqdm
from neural_wrappers.utilities import getGenerators
from neural_wrappers.pytorch import trGetData, npGetData

from plotter import makeNpy

def export_pseudolabels_npy(model, reader, batchSize):
	targetNode = model.reduceLink.outputNode
	cnt = 0
	generator, numSteps = getGenerators(reader, batchSize, maxPrefetch=1, keys=["validation"])

	for i in tqdm(range(numSteps)):
		data = next(generator)[0]
		trLabels = trGetData(data)
		for node in model.nodes:
			node.messages = {}
			if node.groundTruthKey in trLabels:
				node.setGroundTruth(trLabels)
		_ = model.npForward(data)

		graphOutput = model.reduceLink.graphOutputs
		thisBlended = npGetData(graphOutput["blended"])
		MB = len(thisBlended)

		for j in range(MB):
			thisNpy = makeNpy(thisBlended[j], targetNode)
			np.save("%d.npy" % cnt, thisNpy)
			cnt += 1
