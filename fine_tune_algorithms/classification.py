# Should be NumPy/Torch generic
def meanProbabilitiesAndThresholdFn(x):
	def toCategorical(y, x):
		yOrigShape = y.shape
		NC = yOrigShape[-1]
		yFlatten = y.flatten().reshape((-1, NC))
		xFlatten = x.flatten()
		yFlatten[range(len(xFlatten)), xFlatten] = 1
		return yFlatten.reshape(*yOrigShape)
	x = x.mean(0)
	# GT will be stored in this new variable
	newGT = x * 0
	# Get highest prediction on last dimension (class)
	x = x.argmax(-1)
	# Transform it to categorical value
	newGT = toCategorical(newGT, x)
	return newGT

def meanProbabilitiesFn(x):
	return x.mean(0)

def majorityFn(x):
	assert False, "TODO"

def getReduceFn(args):
	reduceFunction = {
		"majority" : (majorityFn, majorityFn),
		"mean_probabilities" : (meanProbabilitiesFn, meanProbabilitiesFn), # This is the good one
		"mean_probabilities_and_threshold" : (meanProbabilitiesAndThresholdFn, meanProbabilitiesAndThresholdFn)
	}[args.reduce_function]
	return reduceFunction

def npVarHighestPrediction(x):
	return x.max(axis=-1).var(axis=0)

def trVarHighestPrediction(x):
	return x.max(dim=-1)[0].var(dim=0)

def npMeanClassesVariance(x):
	return x.var(axis=-1).mean(axis=0)

def trMeanClassesVariance(x):
	return x.var(dim=-1).mean(dim=0)

def getChoiceFn(args):
	choiceFunction = {
		"variance_highest_prediction" : (npVarHighestPrediction, trVarHighestPrediction),
		"mean_classes_variance" : (npMeanClassesVariance, trMeanClassesVariance) # This is the good one
	}[args.choice_function]
	return choiceFunction