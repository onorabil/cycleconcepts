import sys
sys.path.append("../")
import numpy as np
import torch as tr
from argparse import ArgumentParser
from functools import partial
from neural_wrappers.utilities import getGenerators, changeDirectory
from neural_wrappers.pytorch import device
from neural_wrappers.callbacks import Callback

from cycleconcepts.reader import Reader
from cycleconcepts.utils import getModelDataDimsAndHyperParams, fullPath, addNodesArguments

from cfgs import *
from single_links_performance import single_links_performance
from algorithms import getAlgorithm, OptimalChoice
from fine_tune import fine_tune
from plotter import saveImages
from export_pseudolabels_npy import export_pseudolabels_npy

class MyCallback(Callback):
	def __init__(self, cfg, reader):
		super().__init__()
		self.res = np.zeros((0, 4), dtype=np.float32)
		self.cfg = cfg
		self.reader = reader
		self.positionsExtremes = {k : reader.baseReader.dataset["others"]["dataStatistics"]["position"][k][0 : 3] \
			for k in reader.baseReader.dataset["others"]["dataStatistics"]["position"]}

	def positionMetric(results, labels, positionsExtremes):
		Min, Max = positionsExtremes["min"], positionsExtremes["max"]
		# Clip output and renormalie to meters. Assume output and targets are in [0:1]
		tPosition = labels[:, 0 : 3]
		yPosition = np.clip(results[:, 0 : 3], 0, 1)

		tPosition = tPosition * (Max - Min) + Min
		yPosition = yPosition * (Max - Min) + Min

		error = np.linalg.norm(tPosition - yPosition, axis=-1)
		return np.expand_dims(error, axis=-1)

	def orientationMetric(results, labels):
		# Assume eulers are in [0 : 1]
		tOrientation = labels[:, 3 : ]
		yOrientation = np.clip(results[:, 3 : ], 0, 1)

		# orientation :: [0 : 1] => [-1 : 1]
		tOrientation = tOrientation * 2 - 1
		yOrientation = yOrientation * 2 - 1

		# https://stackoverflow.com/questions/1878907/the-smallest-difference-between-2-angles
		# [-1 : 1] => [0 : 1]
		error = np.abs((tOrientation - yOrientation + 1) % 2 - 1)

		# error :: [0 : 1] => [0 : 180]
		error = error * 180
		return error

	def onIterationEnd(self, results, labels, **kwargs):
		key = list(results.keys())[-1]
		position = MyCallback.positionMetric(results[key][0], labels["pose_6dof"], self.positionsExtremes)
		orientation = MyCallback.orientationMetric(results[key][0], labels["pose_6dof"])
		thisRes = np.concatenate([position, orientation], axis=-1)
		self.res = np.concatenate([self.res, thisRes], axis=0)

	def onEpochEnd(self, **kwargs):
		print("%s. Mean: %s. Median: %s" % (self.cfg, self.res.mean(axis=0), np.median(self.res, axis=0)))

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("graph_config")
	parser.add_argument("test_dataset_path")
	parser.add_argument("validation_dataset_path")

	# Training stuff
	parser.add_argument("--batch_size", default=5, type=int)
	parser.add_argument("--num_epochs", default=100, type=int)
	parser.add_argument("--lr", default=0.001, type=float)
	parser.add_argument("--dir")
	# Reader stuff
	parser.add_argument("--resolution", default="512,512")

	# Various nodes hyperparameters
	parser = addNodesArguments(parser)

	# Fine tune single link using various graph cfgs
	parser.add_argument("--singleLinksPath", help="Used for CFGs that use pretrained single links")

	# Hyperparameters used by fine tune algorithms
	parser.add_argument("--algorithm")
	parser.add_argument("--numBins", default=100, type=int)
	parser.add_argument("--choice_function")
	parser.add_argument("--reduce_function")

	args = parser.parse_args()
	args.resolution = tuple(map(lambda x : int(x), args.resolution.split(",")))

	args.singleLinksPath = fullPath(args.singleLinksPath)
	args.dir = fullPath(args.dir)
	args.test_dataset_path = fullPath(args.test_dataset_path)
	args.validation_dataset_path = fullPath(args.validation_dataset_path)
	args.dataset_path = args.test_dataset_path

	assert args.type in ("single_links_performance", "test",  "fine_tune", "export_images", \
		"export_video", "export_pseudolabels_npy", "test_variance")
	return args

def main():
	args = getArgs()

	if args.type == "single_links_performance":
		single_links_performance(args)
		exit()

	model, dataDims, hyperParameters = getModelDataDimsAndHyperParams(args, globals()[args.graph_config])

	testReader = Reader(args.test_dataset_path, dataDims=dataDims, hyperParameters=hyperParameters)
	valReader = Reader(args.validation_dataset_path, dataDims=dataDims, hyperParameters=hyperParameters)
	# model.addCallbacks([MyCallback(args.graph_config, testReader)])
	changeDirectory(args.dir)
	print(model.summary())

	algorithm = getAlgorithm(args, model)
	model.reduceLink.algorithm = algorithm
	print("Algorithm used: %s" % (algorithm))
	# Run the optimization algorithm
	if args.type != "export_pseudolabels_npy":
		algorithm.runAlgorithm(valReader, args.batch_size)
		print("Algorithm's theoretical performance:", algorithm.getResults())

	if args.type in ("export_images", "export_video"):
		model.optimalChoice = OptimalChoice(model, reduceFunctionName=args.reduce_function)
		saveImages(model, testReader, args.batch_size, args.type)
	elif args.type == "test_variance":
		model.addCallbacks([VarianceCB(model)])
		generator, numSteps = getGenerators(valReader, args.batch_size, keys=["validation"])
		model.test_generator(generator, numSteps)
	elif args.type == "export_pseudolabels_npy":
		export_pseudolabels_npy(model, testReader, args.batch_size)
	elif args.type == "test":
		generator, numSteps = getGenerators(testReader, args.batch_size, keys=["validation"])
		model.setIterPrintMessageKeys([])
		res = model.test_generator(generator, numSteps, printMessage="v2")
		res = res[str(model.edges[-1])]
		print("Results for target edges: %s" % (model.edges[-1]))
		for key in res:
			print("- %s : %2.8f" % (key, res[key]))
	elif args.type == "fine_tune":
		fine_tune(model, testReader, args.batch_size, args.num_epochs)

if __name__ == "__main__":
	main()
