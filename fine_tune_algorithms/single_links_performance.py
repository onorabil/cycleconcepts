import sys
sys.path.append("../")
from single_links_train.cfgs import *

import os
from neural_wrappers.graph import Node
from neural_wrappers.utilities import getGenerators

from cycleconcepts.utils import getModelDataDimsAndHyperParams
from cycleconcepts.reader import Reader


def getTaskCfgs(task, singleLinkPaths):
	source, dest = task.split("2")
	allSingleLinks = list(filter(lambda x : os.path.isdir("%s/%s" % (singleLinkPaths, x)), \
		os.listdir(singleLinkPaths)))
	relevantSource = list(filter(lambda x : x.startswith(source), allSingleLinks))
	relevantDest = list(filter(lambda x : x.endswith(dest), allSingleLinks))

	final = []
	if task in relevantSource:
		final.append(task)
		relevantSource.remove(task)
	finalDest = list(map(lambda x : "%s2%s" % (x.split("2")[1], dest), relevantSource))
	for i in range(len(finalDest)):
		assert finalDest[i] in relevantDest, "%s vs %s" % (finalDest[i], relevantDest)
		final.append((relevantSource[i], finalDest[i]))

	actualFinal = []
	for cfg in final:
		if cfg == task:
			cfgs = [cfg]
		else:
			assert len(cfg) == 2
			cfgs = [*cfg, "%s2%s" % (cfg[0], cfg[1].split("2")[1])]
		actualFinal.append(cfgs)

	return actualFinal

def doTask(model, datasetPath, batchSize, dataDims, hyperParameters):
	reader = Reader(datasetPath, dataDims, hyperParameters)
	generator, steps = getGenerators(reader, batchSize, keys=["validation"])
	return model.test_generator(generator, steps)

def doCfg(args, cfgs):
	results = {}
	for i, cfg in enumerate(cfgs):
		print("Current cfg:", cfg)
		Node.lastNodeID = 0
		args.graph_config = cfg
		model, dataDims, hyperParameters = getModelDataDimsAndHyperParams(args, globals()[args.graph_config])
		model.addHyperParameters(hyperParameters)
	
		weightsPath = "%s/%s" % (args.singleLinksPath, cfg)
		if os.path.isdir(weightsPath):
			print("Attempting to load from %s" % (weightsPath))
			model.loadWeights("%s/model_best_Loss.pkl" % (weightsPath), yolo=True)

		resValid = doTask(model, args.validation_dataset_path, args.batch_size, dataDims, hyperParameters)
		print("Valid:", resValid)
		resTest = doTask(model, args.test_dataset_path, args.batch_size, dataDims, hyperParameters)
		print("Test:", resTest)
		print("_____________________")
		results[cfg] = {"Validation" : resValid, "Test" : resTest}

		del model
	return results

def single_links_performance(args):
	args.task = args.graph_config
	assert not args.validation_dataset_path is None
	taskCfgs = getTaskCfgs(args.task, args.singleLinksPath)
	print("All tasks:", taskCfgs)

	allResults = {}
	for cfgs in taskCfgs:
		res = doCfg(args, cfgs)
		for key in res:
			assert not key in allResults
			allResults[key] = res[key]
	
	file = open("%s/%s.txt" % (args.singleLinksPath, args.task), "w")
	print("Writing results to %s/%s.txt" % (args.singleLinksPath, args.task))
	file.write(str(allResults))
	file.close()
