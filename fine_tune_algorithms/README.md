# Fine tune algorithms

This subrepository hold the implementation of the algorithms used for creating graph ensembles (any CFG with 2 or more paths to a destination node), testing their results using a series of pretrained single links (called single link banks) as well as exporting pseudo labels for the iterative improvement process

## 1. Complex CFGs

The purpose of this subrepository is to create complex graph configurations and separate them from the single links (or dual hops), which are defined as simple graph configurations. Basically anything that uses 2 paths can be seen as a complex graph cfg. Example: `rgb2depth + rgb2semantic2depth`)

The complex CFGs are built as a series of single links put together. This is done in `cfgs/link_to_improve_ft` subdirectory. We'll look at `rgb2depth` as our link we wish to improve.

That means `RGB` is our input node and `Depth` is our output node. Thus, `Halftone`, `Wireframe`, `Semantic`, `CameraNormal`, `Normal`, `Optical Flow` and `Pose` are our intermediate representations.

## 2. Building the complex CFGs

There are 7 possible intermediate representations, meaning that in total there are 7! possible graph configurations (7 for 3 nodes * 6 for 4 nodes * ...). Obviously this number is too large to be feasible and it is possible that the performances can be approximated well enough with some proxy results.

### 2.1 N * (N - 1) * ... * 1 = N! tests algorithm (exhaustive)

- This is simply the approach where we try all possible combinations of graph CFGs. Smarter algorithms, such as Genetic algorithms or Bayesian optimization can be applied to guide the search in this factorial space.

### 2.2 N + (N - 1) + ... + 1 = N * (N + 1) / 2 tests algorithm (semi-greedy)

- Instead of trying all the possible graph CFGs, we could build, at the first step, all the CFGs with 3 nodes (so rgb2depth + rgb2halftone2depth, rgb2depth + rgb2semantic2depth, ...). We will test these 3 nodes CFGs and benchmark them, keeping only the best result. Then, when we go to 4 nodes, we're going to remove the best node at previous result and we are now left with N-1 possible choices, hence the sum formula.

### 2.3 N tests algorithm (greedy)

- Step 1 Benchmarking each path individually

    The first such proxy result is benchmarking each of the 7 configurations of 2 nodes (rgb2XXX2depth) individually and rank them. Let's look at a concrete example for rgb2depth on a test set.

    | CFG | Result |
    |---|---|
    | rgb2depth  | 4.60  |
    | rgb2halftone2depth  | 3.01  |
    | rgb2semantic2depth  | 5.05  |
    | rgb2cameranormal2depth  | 4.08  |
    | rgb2normal2depth  | 4.32  |
    | rgb2wireframe2depth  | 9.04  |
    | rgb2opticalflow2depth  | 5.20 |
    | rgb2pose2depth  | 10.71 |

    Based on these results, we can rank them like this: `Halftone` > `CameraNormal` > `Normal` > `Semantic` > `Optical Flow` > `Wireframe` > `Pose`. Note that however these individual paths are trained is not relevant here, we are only using pretrained networks and benchmarking them against each other. For algorithms on how to train each path, see single_links_train, in the algorithms section (GT-only vs 50/50 vs noGT vs concomitant).

- Step 2 Greedily create the complex CFGs
    
    Based on the ranking, we are first going to create (rgb2depth + rgb2halftone2depth), then we are going to create (rgb2depth + rgb2halftone2depth + rgb2cameranormal2depth). This goes on until we use all the possible nodes. 

    The implementation of these CFGs is under `cfgs/rgb2depth_ft/rgb2depth_graph_ensemble_iter1.py` and matches the table above.

## 3. Testing & logging results

### 3.1 Graph ensembles

We should observe the complex CFGs as a single link in itself. Basically, what we are trying to achieve is to improve a specific task (say rgb2depth) using a complex graph configuration (say rgb2depth + rgb2halftone2depth). However, if we were to clamp this complex CFG with multiple paths, at the end what we are left is just a single link (one path) with some additional steps.

After we are done with the step above, of building the complex CFGs, we need to test each of this graph in order to realize which one brings the best improvement (if any) over the original single link. For the example above (2.3), we have created 7 complex CFGs, one for 3 nodes, one for 4 nodes and so on.

After testing, we are left with a table like this (N=1 means 3 nodes, so rgb2depth + rgb2halftone2depth based on the ranking above):

| N | Result |
|---|---|
| baseline | 4.60 |
| 1  | 4.09  |
| 2  | 3.28  |
| 3  | 3.40  |
| 4  | 3.11  |
| 5  | 3.37  |
| 6  | 3.28  |
| 7  | 3.70 |

In order to get these results, we're going to use the following command:
```
python main.py test <cfg> </path/to/test.h5> </path/to/validation.h5> --singleLinksPath=</path/to/single_links_bank> --graph_params=<params> --algorithm=<algorithm_used> --algorithm_params=<params>
```

Again, the implementation for the rgb2depth graph ensembles are in `cfgs/rgb2depth_ft/rgb2depth_graph_ensemble_iter1.py`. For N=2, the name is `rgb2depth_graph_ensemble_iter1_one_path` (one additional path in residual mode, so rgb2depth + rgb2halftone2depth). Names are subject of change so this may not reflect reality when you read this.

The `--graph_params` are the hyperparameters used when each single_link was trained and matches the discussion in `single_links` subrepository (--maxDepthMeters for `Depth` etc.). At the time of writing this, some engineering was made to load pretrained graph cfgs and hyperparameters are not checked, so if we trained with `--resolution=256,256` and forget to use it, the default (512,512) will be used and mess up results. This is subject to refactoring.

The `--algorithm` parameter is used in merging the results of each individual path. This is discussed below.

The `--single_links_path` parameter is a path to a directory where each single link can load the pretrained network. For example, for `rgb2depth_graph_ensemble_iter1_one_path` we need to have `rgb2depth`, `rgb2halftone` and `halftone2depth` already pretrained (using a training scheme, No-GT being the most common, but not necessarily). For rgb2depth (no-gt), we have these stored in `single_links_bank/depth/nogt` with links to the results in the `single_links` subrepository, so we don't copy files for no reason and also to reduce risk of using wrong weights.

### 3.2 Graph-in-graph (embedded ensembles CFGs)

## 4. Ensemble algorithms

TODO

### 4.1 Optimal Choice

### 4.2 Reduce Only

### 4.3 Variance Threshold

### 4.4 Statistical Significance

## 5. Distilating knowledge from ensembles to single links

The assumption here is that we are given a training set, let's say for `rgb2depth`, where we have a flight that has synchronized `RGB` and `Depth` data. We train on this data and we get a test set performance, as discussed above. Then, using the other labeled data (`Halftone`, `Semantic` etc.), we find the best ensemble CFG which has a better performance on the test set. We'll use this best CFG to create new labels for our `Depth` node using new flights, since `RGB` data is freely available and we can always make new flights. Basically, we can enrich our original labeled `RGB` and `Depth` train set with a new set of `RGB` and `Depth` data, labeled by the CFG itself.

After merging the two datasets, we can train (from scratch or fine tune the `rgb2depth` single link) in order to leverage the extended dataset.

### 5.1 Building new dataset

WIP: there will be a script that merges two h5 files into one

```python3 todo.py </path/to/dataset_i.h5> </path/to/new_flight.h5> </path/to/dataset_i+1.h5>```

The dataset at step i should have all keys, while the new flight should have only `RGB` data and the other modes will be generated by creating pseudolabels starting from these RGBs. Placeholder names will be put so the pseudolabels can be simply copied in the expected paths.

### 5.2 Exporting pseudolabels

This is done by exporting npy files for each `RGB` input by passing through the pretrained graph and using the predictions as pseudolabels.

```
python main.py export_npy <best_cfg> </path/to/new_flight.h5> --singleLinksPath=</path/to/single_links_bank> --graph_params=<params> --algorithm=<algorithm_used> --algorithm_params=<params>
```

This will export a set of npy files, named `1.npy, ..., N.npy`. Then, we'll just copy these npy files (with symbolic links to expected names) in the dataset i+1's `depth` directory.

### 5.3 Training on new dataset

This is done simply by training (be it from scratch or fine-tuning) the original single link, but using the newly generated and updated dataset with pseudolabels + original labels if available.

```python3 main.py train <original_single_link_cfg> </path/to/dataset_i+1.h5>```