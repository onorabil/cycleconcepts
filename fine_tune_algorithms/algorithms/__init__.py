from .algorithm import Algorithm
from .optimal_choice import OptimalChoice
from .reduce_only import ReduceOnly
from .get_algorithm import getAlgorithm