import os
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
from typing import Optional, Dict
from tqdm import tqdm
from neural_wrappers.readers import DatasetReader
from neural_wrappers.pytorch import trGetData, npGetData, trToNpCall
from neural_wrappers.utilities import getGenerators, RunningMean
from ..algorithm import Algorithm

def doAnalysis(errors, choiceFunctionName, reduceFunctionName):
	numBins = len(errors["errorsBlended"])
	thresholds = np.linspace(0, 1, numBins)
	optimalThreshold = thresholds[np.argmin(errors["errorsBlended"])]

	plt.plot(thresholds, errors["errorsBlended"], label="Errors blended")
	plt.plot(thresholds, [errors["errorSingleLink"]] * numBins, \
		label="Error single link (%2.3f)" % (errors["errorSingleLink"]))
	plt.xlabel("Threshold")
	plt.ylabel("Loss on GT")
	A = list(errors["errorsBlended"]) + [errors["errorSingleLink"]]
	yMin, yMax = np.min(A), np.max(A)
	plt.ylim(yMin - yMin * 0.1, yMax + yMax * 0.1)

	fileName = "statistical_significance_errors_%d_%s_%s.png" % (numBins, choiceFunctionName, reduceFunctionName)
	print("Stored analysis to %s" % (fileName))
	plt.legend()
	plt.savefig(fileName)

def topProbabilityFn(graphValues):
	meanProbabilities = graphValues.mean(dim=0)
	result = meanProbabilities.max(dim=-1)[0]
	return result.unsqueeze(dim=-1)

def topMinusSecondProbabilityFn(graphValues):
	meanProbabilities = graphValues.mean(dim=0)
	topTwo = meanProbabilities.topk(k=2, dim=-1)[0]
	result = topTwo[..., 0] - topTwo[..., 1]
	return result.unsqueeze(dim=-1)

def topProbabilityReduceFn(graphValues):
	meanProbabilities = graphValues.mean(dim=0)
	Max = meanProbabilities.max(dim=-1)[0].unsqueeze(dim=-1)
	return meanProbabilities == Max

class StatisticalSignificance(Algorithm):
	def __init__(self, model, choiceFunctionName, reduceFunctionName, numBins):
		super(StatisticalSignificance, self).__init__(model)
		self.numBins = numBins
		self.choiceFunctionName = choiceFunctionName
		self.reduceFunctionName = reduceFunctionName

		self.choiceFunction = {
			"top_probability" : topProbabilityFn,
			"top_minus_second_probability" : topMinusSecondProbabilityFn
		}[self.choiceFunctionName]

		self.reduceFunction = {
			"mean_probabilities" : lambda x : x.mean(dim=0),
			"top_probability" : topProbabilityReduceFn
		}[self.reduceFunctionName]

		self.errors = None
		self.optimalThreshold = None

	def computeErrors(self, generator, numSteps):
		print("Desired metric: %s" % (self.metricKey))
		# Keep a running mean for the errors of all possible thresholds
		errors = {
			"errorSingleLink" : RunningMean(0),
			"errorsBlended" : RunningMean(np.zeros(self.numBins, dtype=np.float32))
		}

		thresholds = np.linspace(0, 1, self.numBins)
		for i in tqdm(range(numSteps), desc="Computing errors"):
			data = next(generator)[0]
			# Pass through the graph
			self.model.iterationEpilogue(isTraining=False, isOptimizing=False, trLabels=trGetData(data))
			res = self.model.npForward(data)[self.strTargetEdge][0]

			thisGT = npGetData(self.targetNode.getGroundTruth())
			thisChoices = res["choices"]
			thisGraph = res["singleLinksMerged"]
			thisSingleLink = res["originalSingleLink"]
			MB = len(thisGT)

			errors["errorSingleLink"].update(trToNpCall(self.metric, thisSingleLink, thisGT) * MB, MB)
			blendedErrors = np.zeros((self.numBins, ), dtype=np.float32)
			for j in range(self.numBins):
				thisMask = thisChoices >= thresholds[j]
				thisBlended = thisMask * thisGraph + (~thisMask) * thisSingleLink
				blendedErrors[j] = trToNpCall(self.metric, thisBlended, thisGT)
			errors["errorsBlended"].update(blendedErrors * MB, MB)
		return {k : errors[k].get() for k in errors}

	def getErrors(self, reader, batchSize):
		fileName = "statistical_significance_errors_%d_%s_%s.npy" % \
			(self.numBins, self.choiceFunctionName, self.reduceFunctionName)
		if os.path.isfile(fileName):
			print("Loading statistical significance errors from %s" % (fileName))
			items = np.load(fileName, allow_pickle=True).item()
			assert items["graphStr"] == str(self.model), "%s \nvs \n%s" % (items["graphStr"], str(self.model))	
			assert items["choiceFunction"] == self.choiceFunctionName, \
				"%s vs %s" % (items["choiceFunction"], self.choiceFunctionName)
			assert items["reduceFunction"] == self.reduceFunctionName, \
				"%s vs %s" % (items["reduceFunction"], self.reduceFunctionName)
			errors = items["errors"]
		else:
			generator, numSteps = getGenerators(reader, batchSize, keys=["validation"])
			errors = self.computeErrors(generator, numSteps)
			print("Stored statistical significance errors at %s" % (fileName))
			np.save(fileName, {
				"errors" : errors,
				"graphStr" : str(self.model),
				"choiceFunction" : self.choiceFunctionName,
				"reduceFunction" : self.reduceFunctionName
			})
		return errors

	def runAlgorithm(self, reader : DatasetReader, batchSize : int):
		print("[%s] Running algorithm." % (str(self)))
		self.errors = self.getErrors(reader, batchSize)
		doAnalysis(self.errors, self.choiceFunctionName, self.reduceFunctionName)

		thresholds = np.linspace(0, 1, self.numBins)
		self.optimalThreshold = thresholds[np.argmin(self.errors["errorsBlended"])]

	# As choice function, we just compute the mean of the probabilities. We'll use this to compute our mask.
	def getChoices(self, graphValues : tr.Tensor, singleLinkValues : tr.Tensor, GT: tr.Tensor) -> tr.Tensor:
		return self.choiceFunction(graphValues)

	def getMask(self, choices : tr.Tensor) -> Optional[tr.Tensor]:
		if self.optimalThreshold is None:
			return None
		else:
			return choices >= self.optimalThreshold

	def getMerged(self, graphValues : tr.Tensor) -> Optional[tr.Tensor]:
		# TODO: Merged 0/1 vs value.
		return self.reduceFunction(graphValues)

	def getResults(self) -> Dict[str, float]:
		assert not self.errors is None
		bestError = np.min(self.errors["errorsBlended"])
		return {
			"Statistical Signifiance Error" : bestError, \
			"Single link error" : self.errors["errorSingleLink"],
			"Optimal threshold" : self.optimalThreshold
		}

	def __str__(self):
		return "Statistical Significance (Choice: %s. Reduce: %s. Bins: %d)" % \
			(self.choiceFunctionName, self.reduceFunctionName, self.numBins)
