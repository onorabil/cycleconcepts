from ..regression import VarianceThreshold

class ClassificationVarianceThreshold(VarianceThreshold):
	def __init__(self, model, choiceFunctionName, reduceFunctionName, numBins):
		super(VarianceThreshold, self).__init__(model)
		self.choiceFunctionName = choiceFunctionName
		self.reduceFunctionName = reduceFunctionName

		self.reduceFunction = {
			"mean_probabilities" : lambda x : x.mean(dim=0)
		}[self.reduceFunctionName]

		self.choiceFunction = {
			"mean_classes_variance" : lambda x : x.var(dim=-1).mean(dim=0).unsqueeze(dim=-1)
		}[self.choiceFunctionName]
		self.numBins = numBins

		self.variances = None
		self.errors = None
		self.optimalThreshold = None

	def __str__(self) -> str:
		return "Classification Variance Threshold (Choice: %s. Reduce: %s. Bins: %d)" % \
			(self.choiceFunctionName, self.reduceFunctionName, self.numBins)
