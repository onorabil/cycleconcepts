from .algorithm import Algorithm
from argparse import ArgumentParser
from neural_wrappers.graph import Graph
from functools import partial

from .regression import VarianceThreshold
from .classification import ClassificationVarianceThreshold, StatisticalSignificance
from .optimal_choice import OptimalChoice
from .reduce_only import ReduceOnly

def getAlgorithm(args : ArgumentParser, model : Graph) -> Algorithm:
	outputNodeName = model.reduceLink.outputNode.__repr__()
	problemType = {
		"Depth" : "regression",
		"Semantic" : "classification",
		"Wireframe" : "regression",
		"CameraNormal" : "regression",
		"Normal" : "regression",
		"Pose" : "regression"
	}[outputNodeName]

	regressionAlgorithms = {
		"optimal_choice" : lambda: OptimalChoice(model, reduceFunctionName=args.reduce_function),
		"variance_threshold" : lambda: VarianceThreshold(model, choiceFunctionName=args.choice_function, \
			reduceFunctionName=args.reduce_function, numBins=args.numBins),
		"reduce_only" : lambda: ReduceOnly(model, reduceFunctionName=args.reduce_function)
	}

	classificationAlgorithms = {
		"optimal_choice" : lambda: OptimalChoice(model, reduceFunctionName=args.reduce_function),
		"variance_threshold" : lambda: ClassificationVarianceThreshold(model, numBins=args.numBins, \
			choiceFunctionName=args.choice_function, reduceFunctionName=args.reduce_function),
		"statistical_significance" : lambda: StatisticalSignificance(model, numBins=args.numBins, \
			choiceFunctionName=args.choice_function, reduceFunctionName=args.reduce_function), \
		"reduce_only" : lambda: ReduceOnly(model, reduceFunctionName=args.reduce_function)
	}

	algorithm = {
		"regression" : regressionAlgorithms,
		"classification" : classificationAlgorithms
	}[problemType][args.algorithm]()
	return algorithm
