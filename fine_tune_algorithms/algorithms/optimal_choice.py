import os
import numpy as np
import torch as tr
from tqdm import tqdm
from typing import Optional
from functools import partial
from neural_wrappers.readers import DatasetReader
from neural_wrappers.utilities import getGenerators, RunningMean, npGetInfo
from neural_wrappers.pytorch import trGetData, npGetData, trToNpCall, npToTrCall
from .algorithm import Algorithm

def topProbabilityReduceFn(graphValues):
	meanProbabilities = graphValues.mean(dim=0)
	Max = meanProbabilities.max(dim=-1)[0].unsqueeze(dim=-1)
	return meanProbabilities == Max

class OptimalChoice(Algorithm):
	def __init__(self, model, reduceFunctionName):
		super().__init__(model)
		self.reduceFunctionName = reduceFunctionName

		self.reduceFunction = {
			"median" : lambda x : x.median(dim=0)[0],
			"mean" : lambda x : x.mean(dim=0),
			"mean_probabilities" : lambda x : x.mean(dim=0),
			"top_probability" : topProbabilityReduceFn
		}[self.reduceFunctionName]

		def f(y, t):
			# return (y.argmax(-1) == t.argmax(-1)).type(tr.uint8)
			Shape = y.shape[0 : -1]
			y = y[t.type(tr.bool)]
			y = y.reshape(Shape).unsqueeze(dim=-1)
			y = -tr.log(y + 1e-5)
			return y

		self.pixelMetric = {
			"Depth" : lambda y, t : (y - t).abs() * self.targetNode.maxDepthMeters,
			"Semantic" : f,
			"Wireframe" : lambda y, t : (y - t) ** 2,
			"CameraNormal" : lambda y, t : (y - t).abs() * 360,
			"Normal" : lambda y, t : (y - t).abs() * 360,
			"Pose" : lambda y, t : (y - t).abs()
		}[self.targetNode.__repr__()]
		self.blendedPerformance = None
		self.singleLinkPerformance = None
		self.fileName = "optimal_%s.npy" % (self.reduceFunctionName)

	def computeErrors(self, generator, numSteps):
		blendedPerformance = RunningMean(0)
		singleLinkPerformance = RunningMean(0)

		for i in tqdm(range(numSteps), desc="Computing errors"):
			data = next(generator)[0]
			self.model.iterationEpilogue(isTraining=False, isOptimizing=False, trLabels=trGetData(data))
			res = self.model.npForward(data)

			graphOutput = self.reduceLink.graphOutputs
			MB = graphOutput["choices"].shape[0]
			thisGT = npGetData(self.targetNode.getGroundTruth())

			# breakpoint()
			singleLinkMetric = npToTrCall(self.metric, graphOutput["originalSingleLink"], thisGT)
			blendedMetric = npToTrCall(self.metric, graphOutput["blended"], thisGT)
			blendedPerformance.update(blendedMetric * MB, MB)
			singleLinkPerformance.update(singleLinkMetric * MB, MB)
	
		return blendedPerformance.get(), singleLinkPerformance.get()

	def runAlgorithm(self, reader : DatasetReader, batchSize : int):
		if os.path.isfile(self.fileName):
			print("Loading optimal algorithm's errors from %s" % (self.fileName))
			items = np.load(self.fileName, allow_pickle=True).item()
			errors = items["errors"]
			assert items["reduceFunction"] == self.reduceFunctionName, "%s vs %s" % \
				(items["reduceFunction"], self.reduceFunctionName)
			assert items["graphStr"] == str(self.subGraph), "%s \nvs \n%s" % \
				(items["graphStr"], str(self.subGraph))
		else:
			generator, numSteps = getGenerators(reader, batchSize, keys=["validation"])
			errors = self.computeErrors(generator, numSteps)
			print("Stored optimal algorithm's results at %s" % (self.fileName))
			np.save(self.fileName, {
				"errors" : errors,
				"reduceFunction" : self.reduceFunctionName,
				"graphStr" : str(self.subGraph)
			})
		self.blendedPerformance, self.singleLinkPerformance = errors

	def getChoices(self, graphValues : tr.Tensor, singleLinkValues : tr.Tensor, GT: tr.Tensor) \
		-> tr.Tensor:
		diffError = self.getDiffError(self.reduceFunction(graphValues), singleLinkValues, GT)
		# Keep only pixels where there is improvement
		diffError[diffError < 0] = 0
		return diffError

	def getMask(self, choices : tr.Tensor) -> Optional[tr.Tensor]:
		return choices > 0

	def getMerged(self, graphValues):
		return self.reduceFunction(graphValues)

	def getDiffError(self, reducedGraphValues, singleLinkValues, GT):
		try:
			errorSL = self.pixelMetric(singleLinkValues, GT)
			errorGraph = self.pixelMetric(reducedGraphValues, GT)
			# Compute the difference between the errors, and where there is better to use the SL, ignore.
			diffError = errorSL - errorGraph
		except Exception:
			# Mostly used for export_psueodlabels_npy where we don't have GT, thus can't compute errors.
			diffError = reducedGraphValues.mean() * 0
		return diffError

	def getResults(self):
		assert not self.blendedPerformance is None, "Run algorithm first."
		return {"Optimal CFG Error" : self.blendedPerformance, "Single link error" : self.singleLinkPerformance}

	def __str__(self):
		return (("[Algorithm] Optimal algorithm. Problem: %s. Target Edge: %s. Metric: %s. ") + \
			("Direction: %s. Reduce function: %s")) % (self.problemType, self.strTargetEdge, self.metricKey, \
			self.metricDirection, self.reduceFunctionName)
