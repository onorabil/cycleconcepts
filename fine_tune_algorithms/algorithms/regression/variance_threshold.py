import os
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from tqdm import tqdm
from typing import Optional, Iterator, Dict
from neural_wrappers.utilities import getGenerators, RunningMean, npCloseEnough
from neural_wrappers.pytorch import trGetData, npGetData, trToNpCall
from neural_wrappers.readers import DatasetReader

from ..algorithm import Algorithm

# Save a plot of variances as well
def plotVarianceThresholds(fileName, variances, numBins):
	variances = np.clip(variances, 0, (variances[-2] + variances[-3] / 2 ))
	plt.plot(np.arange(0, numBins) / numBins * 100, variances)
	plt.xlabel("Percent image")
	plt.ylabel("Variance")
	print("Stored variance thresholds plots at %s" % (fileName))
	plt.savefig(fileName)

# Part 2 stuff (theoretical improvement threshold)
def computeThisError(resultGraph, resultSingleLink, GT, graphVariances, varianceThresholds, metricFn):
	MB = resultGraph.shape[0]
	numThresholds = len(varianceThresholds)
	errors = {
		"errorMaskedSingleLink" : np.zeros((MB, numThresholds), dtype=np.float32),
		"errorMaskedGraph" : np.zeros((MB, numThresholds), dtype=np.float32),
		"errorUnmasked" : np.zeros((MB, numThresholds), dtype=np.float32),
		"errorBlended" : np.zeros((MB, numThresholds), dtype=np.float32)
	}
	# masks :: MB x numThresholds x *imageShape
	masks = np.stack([graphVariances <= threshold for threshold in varianceThresholds], axis=1)
	# masks = np.expand_dims(masks, axis=-1)
	# Vectorize that bitch
	allBlended = np.expand_dims(resultGraph, axis=1) * masks + np.expand_dims(resultSingleLink, axis=1) * ~masks

	for i in range(MB):
		itemGraph = resultGraph[i]
		itemSingleLink = resultSingleLink[i]
		itemGT = GT[i]
		for j in range(numThresholds):
			mask = masks[i, j]
			# Probably should fix this somehow else (i.e. depth has 1 channel as well as mask)
			if mask.shape[-1] == 1:
				mask = mask[..., 0]

			# Mask and add a fake batch of 1 for safety.
			maskedGT = np.expand_dims(itemGT[mask], axis=0)
			maskedSingleLink = np.expand_dims(itemSingleLink[mask], axis=0)
			maskedGraph = np.expand_dims(itemGraph[mask], axis=0)

			# Call the metric for all combinations we care about
			if mask.sum() > 0:
				errors["errorMaskedSingleLink"][i, j] = trToNpCall(metricFn, maskedSingleLink, maskedGT)
				errors["errorMaskedGraph"][i, j] = trToNpCall(metricFn, maskedGraph, maskedGT)
			if mask.sum() > 0 and (~mask).sum() > 0:
				errors["errorBlended"][i, j] = trToNpCall(metricFn, allBlended[i, j], itemGT)
	return errors

# Part 2 plotting stuff
def part2Analysis(errors, variances, metricDirection, metricKey, reduceFunctionName, numBins):
	singleLinkBaseline = errors["errorMaskedSingleLink"][-1]
	print("Baseline single link: %2.4f. Metric: %s (%s)" % (singleLinkBaseline, metricKey, metricDirection))

	errors["errorBlended"][0] = errors["errorBlended"][1]
	errors["errorBlended"][-1] = errors["errorBlended"][-2]
	bestIx = np.argmin(errors["errorBlended"]) if metricDirection == "min" else np.argmax(errors["errorBlended"])

	theoreticalErrorName = "variance_threshold_theoretical_error_%s_%d.png" % (reduceFunctionName, numBins)
	plotPart2MetricVsBaseline(theoreticalErrorName, singleLinkBaseline, errors["errorBlended"], \
		bestIx, metricKey[1], metricDirection, variances)
	maskedErrorName = "variance_threshold_masked_error_%s_%d.png" % (reduceFunctionName, numBins)
	plotPart2MaskedVsSingleLink(maskedErrorName, errors["errorMaskedGraph"], errors["errorMaskedSingleLink"], \
		bestIx, metricKey, metricDirection, variances)

# Baseline as a single line (single link performance) vs masked theoretical improvement + SL on ~mask.
def plotPart2MetricVsBaseline(fileName, baseline, errorBlended, bestIx, metric, metricDirection, variances):
	N = len(errorBlended)

	plt.figure()
	X = np.arange(N)
	plt.plot(X, [baseline] * N, label="Baseline single link: %2.3f" % (baseline))
	plt.plot(X, errorBlended, label="Theoretical improvement using graph")
	plt.xlabel("Varianace @ % of image")
	plt.ylabel(metric)
	plt.legend()

	bestDelta = errorBlended[bestIx]
	bestPercent = bestIx / N * 100
	bestVariance = variances[bestIx]
	title = "Best: %2.3f\nPercent image:%2.2f\nVariance:%2.7f" % (bestDelta, bestPercent, bestVariance)
	plt.annotate(title, xy=(bestPercent + 1, bestDelta))
	plt.plot([bestPercent + 1], [bestDelta], "o")

	yMin, yMax = np.min(list(errorBlended) + [baseline]), np.max(list(errorBlended) + [baseline])
	plt.ylim(yMin - yMin * 0.1, yMax + yMax * 0.1)
	print("[plotPart2MetricVsBaseline] Saved figure at %s" % (fileName))
	plt.savefig(fileName)

def plotPart2MaskedVsSingleLink(fileName, errorGraph, errorSingleLink, bestIx, metric, metricDirection, variances):
	plt.figure()
	X = np.arange(len(errorGraph))

	plt.plot(X[1 : ], errorGraph[1 : ], label="Error through graph")
	plt.plot(X[1 :], errorSingleLink[1 : ], label="Error single link")
	for i in range(1, len(errorGraph) - 1):
		if metricDirection == "max":
			color = "green" if errorGraph[i] > errorSingleLink[i] else "red"
		else:
			color = "red" if errorGraph[i] > errorSingleLink[i] else "green"

		A = X[i], min(errorGraph[i], errorSingleLink[i])
		B = X[i + 1], min(errorGraph[i+1], errorSingleLink[i+1])
		C = X[i + 1], max(errorGraph[i+1], errorSingleLink[i+1])
		D = X[i], max(errorGraph[i], errorSingleLink[i])
		poly = Polygon([A, B, C, D], fill=True, color=color)
		plt.gca().add_patch(poly)

	bestError = min(errorGraph[bestIx], errorSingleLink[bestIx])
	bestDelta = abs(errorGraph[bestIx] - errorSingleLink[bestIx])
	bestPercent = bestIx / len(errorGraph) * 100
	bestVariance = variances[bestIx]
	plt.plot([bestPercent + 1], [bestError], "x", color="red")
	title = "Best: %2.3f. Diff: %2.3f. Percent image:%2.2f. Variance:%2.7f." % \
		(bestError, bestDelta, bestPercent, bestVariance)
	plt.title(title)

	plt.xlabel("Varianace @ % of image")
	plt.ylabel(metric)
	plt.legend()
	print("[plotPart2MaskedVsSingleLink] Saved figure at %s" % (fileName))
	plt.savefig(fileName)

class VarianceThreshold(Algorithm):
	def __init__(self, model, choiceFunctionName, reduceFunctionName, numBins):
		super().__init__(model)
		self.choiceFunctionName = choiceFunctionName
		self.reduceFunctionName = reduceFunctionName

		self.reduceFunction = {
			"median" : lambda x : x.median(dim=0)[0],
			"mean" : lambda x : x.mean(dim=0)
		}[self.reduceFunctionName]
		self.choiceFunction = {
			"variance" : lambda x : x.var(dim=0)
		}[self.choiceFunctionName]

		assert numBins > 10
		self.numBins = numBins

		self.variances = None
		self.errors = None
		self.optimalThreshold = None

	def computeVariances(self, generator : Iterator, numSteps : int) -> np.ndarray:
		runningMean = RunningMean(np.zeros(self.numBins, dtype=np.float32))

		for i in tqdm(range(numSteps), desc="Exporting variance thresholds"):
			data = next(generator)[0]
			# Pass through the graph
			self.model.iterationEpilogue(isTraining=False, isOptimizing=False, trLabels=trGetData(data))
			_ = self.model.npForward(data)
			# Retrieve the the variances of the last edge
			graphOutput = self.reduceLink.graphOutputs

			vars = npGetData(graphOutput["choices"])
			MB = vars.shape[0]
			# MB x imageShape => MB x flattenedImageShape
			vars = vars.reshape((MB, -1))
			# Sort each element of the MB alone
			vars = np.sort(vars, axis=-1)
			# Get the indices we want to retrieve from the sorted variances to get the value of each bin
			Range = np.arange(0, self.numBins) * (vars.shape[-1] // self.numBins)
			Range = np.append(Range[0 : -1], vars.shape[-1] - 1)
			# Retrieve those variances, but set the first one to 0 for consistency.
			vars = vars[:, Range]
			vars[:, 0] = 0
			vars[:, -1] = 1 << 31
			runningMean.updateBatch(vars)

		print("")
		result = runningMean.get()
		assert np.abs(np.sort(result) - result).sum() < 1e-5
		return result

	def getVariances(self, reader : DatasetReader, batchSize : int) -> np.ndarray:
		fileName = "variance_threshold_variances_%s_%d.npy" % (self.reduceFunctionName, self.numBins)
		pngFileName = "variance_threshold_variances_%s_%d.png" % (self.reduceFunctionName, self.numBins)
		if os.path.isfile(fileName):
			items = np.load(fileName, allow_pickle=True).item()
			assert items["graphStr"] == str(self.model), "%s \nvs \n%s" % (items["graphStr"], str(self.model))
			assert items["choiceFunction"] == self.choiceFunctionName, \
				"%s vs %s" % (items["choiceFunction"], self.choiceFunctionName)
			assert items["reduceFunction"] == self.reduceFunctionName, \
				"%s vs %s" % (items["reduceFunction"], self.reduceFunctionName)
			variances = items["variances"]
			assert len(variances) == self.numBins, "%d vs. %d" % (len(variances), self.numBins)
			print("Loaded variances from %s (%d bins)" % (fileName, len(variances)))
		else:
			generator, numSteps = getGenerators(reader, batchSize, keys=["validation"])
			variances = self.computeVariances(generator, numSteps)
			print("Stored variances at %s (%d bins)" % (fileName, len(variances)))
			np.save(fileName, {
				"variances" : variances,
				"choiceFunction" : self.choiceFunctionName,
				"reduceFunction" : self.reduceFunctionName,
				"graphStr" : str(self.model)
			})
		plotVarianceThresholds(pngFileName, variances, self.numBins)
		return variances

	# Go through the validation set once and compute the metric's errors (in average) for all possible thresholds
	def computeErrors(self, generator, numSteps):
		print("Desired metric: %s" % (self.metricKey))

		# Keep a running mean for the errors of all possible thresholds
		errors = {
			"errorMaskedSingleLink" : RunningMean(np.zeros(self.numBins, dtype=np.float32)),
			"errorMaskedGraph" : RunningMean(np.zeros(self.numBins, dtype=np.float32)),
			"errorUnmasked" : RunningMean(np.zeros(self.numBins, dtype=np.float32)),
			"errorBlended" : RunningMean(np.zeros(self.numBins, dtype=np.float32))
		}
		for i in tqdm(range(numSteps), desc="Computing errors"):
			data = next(generator)[0]
			self.model.iterationEpilogue(isTraining=False, isOptimizing=False, trLabels=trGetData(data))
			_ = self.model.npForward(data)

			graphOutput = self.reduceLink.graphOutputs
			thisGT = npGetData(self.targetNode.getGroundTruth())
			thisVar = npGetData(graphOutput["choices"])
			thisGraph = npGetData(graphOutput["singleLinksMerged"])
			thisSingleLink = npGetData(graphOutput["originalSingleLink"])

			thisError = computeThisError(thisGraph, thisSingleLink, thisGT, thisVar, self.variances, self.metric)
			for k in errors:
				errors[k].updateBatch(thisError[k])
			# break
		return {k : errors[k].get() for k in errors}

	def getErrors(self, reader : DatasetReader, batchSize : int) -> Dict[str, np.ndarray]:
		fileName = "variance_threshold_errors_%s_%d.npy" % (self.reduceFunctionName, self.numBins)
		if os.path.isfile(fileName):
			print("Loading variance threshold errors from %s" % (fileName))
			items = np.load(fileName, allow_pickle=True).item()
			assert items["reduceFunction"] == self.reduceFunctionName, "%s vs %s" % \
				(items["reduceFunction"], self.reduceFunctionName)
			assert items["graphStr"] == str(self.model), "%s \nvs \n%s" % (errors["graphStr"], str(self.model))		
			errors = items["errors"]
		else:
			generator, numSteps = getGenerators(reader, batchSize, keys=["validation"])
			errors = self.computeErrors(generator, numSteps)
			print("Stored variance threshold errors at %s" % (fileName))
			np.save(fileName, {
				"errors" : errors,
				"reduceFunction" : self.reduceFunctionName,
				"graphStr" : str(self.model)
			})

		part2Analysis(errors, self.variances, self.metricDirection, self.metricKey, \
			self.reduceFunctionName, self.numBins)
		return errors

	def runAlgorithm(self, reader : DatasetReader, batchSize : int):
		print("[%s] Running algorithm." % (str(self)))
		# Part 1, load or compute the varianes of this dataset
		self.variances = self.getVariances(reader, batchSize)
		print("[%s] Done computing variances." % (str(self)))

		# Part 2. Compute the errors for each variance bin
		self.errors = self.getErrors(reader, batchSize)
		print("[%s] Done computing errors." % (str(self)))

		# Now, we can set up the optimal threshold based on the errors
		# errorsDiff = self.errors["errorMaskedGraph"] - self.errors["errorMaskedSingleLink"]
		bestIx = {"min" : np.argmin, "max" : np.argmax}[self.metricDirection](self.errors["errorBlended"])
		self.optimalThreshold = self.variances[bestIx]
		print("[%s] Set up model's variance threshold at %2.8f." % (str(self), self.variances[bestIx]))

	def getChoices(self, graphValues : tr.Tensor, singleLinkValues : tr.Tensor, GT: tr.Tensor) -> tr.Tensor:
		return self.choiceFunction(graphValues)

	def getMask(self, choices : tr.Tensor) -> Optional[tr.Tensor]:
		if self.optimalThreshold is None:
			return None
		else:
			return choices <= self.optimalThreshold

	def getMerged(self, values : tr.Tensor) -> tr.Tensor:
		return self.reduceFunction(values)

	def getResults(self) -> Dict[str, float]:
		assert not self.errors is None
		bestError = {"min" : np.min, "max" : np.max}[self.metricDirection](self.errors["errorBlended"])
		return {
			"Variance Threshold Error" : bestError, \
			"Single link error" : self.errors["errorMaskedSingleLink"][-1]
		}

	def __str__(self) -> str:
		return "Variance Threshold (Choice: %s. Reduce: %s. Bins: %d)" % \
			(self.choiceFunctionName, self.reduceFunctionName, self.numBins)
