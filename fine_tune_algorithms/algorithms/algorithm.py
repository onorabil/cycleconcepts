import torch as tr
from typing import Optional, Dict
from abc import ABC, abstractmethod
from neural_wrappers.readers import DatasetReader

class Algorithm(ABC):
	def __init__(self, model):
		self.model = model

		self.reduceLinkKey = self.model.reduceLinkKey
		self.reduceLink = self.model.reduceLink
		self.subGraph = self.model.getSubgraph()
		self.strTargetEdge = str(self.reduceLink)
		self.targetNode = self.reduceLink.outputNode

		self.metricKey = "%s Loss" % type(self.reduceLink.inputNode)
		self.metric = self.reduceLink.inputNode.getCriterion()
		self.metricDirection = "min"

		self.problemType = {
			"Depth" : "regression",
			"Semantic" : "classification",
			"Wireframe" : "regression",
			"CameraNormal" : "regression",
			"Normal" : "regression",
			"Pose" : "regression"
		}[self.targetNode.__repr__()]


	def runAlgorithm(self, reader : DatasetReader, batchSize : int):
		pass

	def getChoices(self, graphValues : tr.Tensor, singleLinkValues : tr.Tensor, GT: tr.Tensor) -> tr.Tensor:
		pass

	def getMask(self, choices : tr.Tensor) -> Optional[tr.Tensor]:
		pass

	def getMerged(self, graphValues : tr.Tensor) -> Optional[tr.Tensor]:
		pass

	def getResults(self) -> Dict[str, float]:
		pass

	def __str__(self):
		return "[Algorithm] Generic fine tune on test set. Problem: %s. Target Edge: %s. Metric: %s. Direction: %s" \
			% (self.problemType, self.strTargetEdge, self.metricKey, self.metricDirection)
