import os
import numpy as np
import torch as tr
from neural_wrappers.readers import DatasetReader
from neural_wrappers.utilities import getGenerators, RunningMean, npGetInfo
from neural_wrappers.pytorch import trGetData, npGetData
from tqdm import tqdm
from typing import Optional
from .algorithm import Algorithm
from .optimal_choice import OptimalChoice

class ReduceOnly(OptimalChoice):
	def __init__(self, model, reduceFunctionName):
		super().__init__(model, reduceFunctionName)
		self.fileName = "reduce_only_%s.npy" % (self.reduceFunctionName)

	def getChoices(self, graphValues : tr.Tensor, singleLinkValues : tr.Tensor, GT: tr.Tensor) \
		-> tr.Tensor:
		diffError = self.getDiffError(self.reduceFunction(graphValues), singleLinkValues, GT)
		return diffError * 0 + 1

	def getMask(self, choices : tr.Tensor) -> Optional[tr.Tensor]:
		return choices.type(tr.bool)

	def getMerged(self, graphValues):
		return self.reduceFunction(graphValues)

	def getResults(self):
		assert not self.blendedPerformance is None, "Run algorithm first."
		return {"Reduce Only CFG Error" : self.blendedPerformance, "Single link error" : self.singleLinkPerformance}

	def __str__(self):
		return (("[Algorithm] Reduce Only. Problem: %s. Target Edge: %s. Metric: %s. ") + \
			("Direction: %s. Reduce function: %s")) % (self.problemType, self.strTargetEdge, self.metricKey, \
			self.metricDirection, self.reduceFunctionName)