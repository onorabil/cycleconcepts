import numpy as np
import torch.optim as optim
from functools import partial
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetrics
from neural_wrappers.utilities import getGenerators

def fine_tune(model, reader, batchSize, numEpochs):
	generators = getGenerators(reader, batchSize, maxPrefetch=1, keys=["train", "validation"])
	assert generators[1] == generators[3]

	model.clearCallbacks()
	edgeMetricNames, edgeDirections = [], []
	for name, callback in model.edges[-1].getMetrics().items():
		edgeMetricNames.append((str(model.edges[-1]), name))
		edgeDirections.append(callback.getDirection())
	saveModels = [SaveModels("best", name, direction) for (name, direction) in zip(edgeMetricNames, edgeDirections)]
	model.addCallbacks([SaveHistory("history.txt"), *saveModels, PlotMetrics(edgeMetricNames, edgeDirections)])
	model.setOptimizer(optim.SGD, lr=0.001)
	print(model.summary())

	valGenerator, valNumSteps = getGenerators(reader, batchSize, maxPrefetch=1, keys=["train"])
	print("_______________________")
	res = model.test_generator(valGenerator, stepsPerEpoch=valNumSteps, printMessage="v2")
	for key in res:
		print(key, res[key])
	print("_______________________")

	valGenerator, valNumSteps = getGenerators(reader, batchSize, maxPrefetch=1, keys=["train"])
	generator, numSteps = getGenerators(reader, batchSize, maxPrefetch=1, keys=["train"])
	model.train_generator(generator, numSteps, numEpochs, valGenerator, valNumSteps)

	valGenerator, valNumSteps = getGenerators(reader, batchSize, maxPrefetch=1, keys=["train"])
	print("_______________________")
	res = model.test_generator(valGenerator, stepsPerEpoch=valNumSteps, printMessage="v2")
	for key in res:
		print(key, res[key])
	print("_______________________")
