import numpy as np
from functools import partial

def getChoiceFn(args):
	assert args.choice_function == "variance"
	return lambda x : x.var(axis=0), lambda x : x.var(dim=0).squeeze(dim=-1)

def getReduceFn(args):
	reduceFunction = {
		"mean" : partial(np.mean, axis=0),
		"median" : partial(np.median, axis=0),
	}[args.reduce_function]

	torchReduceFunction = {
		"mean" : lambda x : x.mean(dim=0),
		"median" : lambda x : x.median(dim=0)[0],
	}[args.reduce_function]

	return reduceFunction, torchReduceFunction
