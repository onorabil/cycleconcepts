from typing import Dict, Any
from neural_wrappers.graph import Graph

from ..rgb2semantic_ft import rgb2semantic_graph_ensemble_iter1_two_paths
from ..common import buildSubgraph, buildFineTune, Depth

from algorithms import ReduceOnly

# Graph in graph using rgb2depth's best iter1, but switching rgb2semantic with rgb2semantic's best iter1
# This results in an embedded architecture that should improve even further the performance of rgb2depth
def rgb2depth_graph_ensemble_iter2(hyperParameters : Dict[str, Any]) -> Graph:
    rgb2semantic_iter1 = rgb2semantic_graph_ensemble_iter1_two_paths(hyperParameters)
    rgb2semantic_iter1.reduceLink.algorithm = ReduceOnly(rgb2semantic_iter1, "mean_probabilities")

    firstEdges = ["rgb2halftone", "rgb2cameranormal", "rgb2normal", "rgb2depth"]
    secondEdges = ["halftone2depth", "semantic2depth", "cameranormal2depth", "normal2depth"]
    rgb2depth_iter1 = buildSubgraph(firstEdges, secondEdges, hyperParameters)

    iter2SubGraph = Graph([
        rgb2semantic_iter1,
        rgb2depth_iter1
    ])
    iter2SubGraph.reduceLink = rgb2depth_iter1.reduceLink
    iter2SubGraph.reduceLinkKey = (rgb2depth_iter1, )
    iter2SubGraph.getSubgraph = rgb2depth_iter1.getSubgraph

    return iter2SubGraph