from typing import Dict, Any
from neural_wrappers.graph import Graph
from neural_wrappers.graph import forwardUseGT, forwardUseIntermediateResult

from ..common import EdgeManager, buildSubgraph

cfgs = {
	"one_path" : [
		["rgb2halftone", "rgb2depth"],
		["halftone2depth"]
	],
	"two_paths" : [
		["rgb2halftone", "rgb2semantic", "rgb2depth"],
		["halftone2depth", "semantic2depth"]
	],
	"three_paths" : [
		["rgb2halftone", "rgb2semantic", "rgb2cameranormal", "rgb2depth"],
		["halftone2depth", "semantic2depth", "cameranormal2depth"]
	],
	"four_paths" : [
		["rgb2halftone", "rgb2semantic", "rgb2cameranormal", "rgb2normal", "rgb2depth"],
		["halftone2depth", "semantic2depth", "cameranormal2depth", "normal2depth"]
	],
	"five_paths" : [
		["rgb2semantic", "rgb2normal", "rgb2cameranormal", "rgb2halftone", "rgb2opticalflow", "rgb2depth"],
		["semantic2depth", "normal2depth", "cameranormal2depth", "halftone2depth", "opticalflow2depth"]
	],
	"six_paths" : [
		["rgb2semantic", "rgb2normal", "rgb2cameranormal", "rgb2halftone", "rgb2opticalflow", "rgb2wireframe", "rgb2depth"],
		["semantic2depth", "normal2depth", "cameranormal2depth", "halftone2depth", "opticalflow2depth", "wireframe2depth"]
	],
	"seven_paths" : [
		["rgb2semantic", "rgb2normal", "rgb2cameranormal", "rgb2halftone", "rgb2opticalflow", "rgb2wireframe", "rgb2pose", "rgb2depth"],
		["semantic2depth", "normal2depth", "cameranormal2depth", "halftone2depth", "opticalflow2depth", "wireframe2depth"]
	]
}

def rgb2depth_graph_ensemble_iter1_one_path(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["one_path"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2depth_graph_ensemble_iter1_two_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["two_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2depth_graph_ensemble_iter1_three_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["three_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2depth_graph_ensemble_iter1_four_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["four_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2depth_graph_ensemble_iter1_five_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["five_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2depth_graph_ensemble_iter1_six_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["six_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2depth_graph_ensemble_iter1_seven_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["seven_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)
