from typing import Dict, Any
from neural_wrappers.graph import Graph
from neural_wrappers.graph import forwardUseGT, forwardUseIntermediateResult

from ..common import EdgeManager, buildSubgraph

cfgs = {
	"one_path" : [
		["rgb2cameranormal", "rgb2pose"],
		["cameranormal2pose"]
	],
	"two_paths" : [
		["rgb2cameranormal", "rgb2normal", "rgb2pose"],
		["cameranormal2pose", "normal2pose"]
	],
	"three_paths" : [
		["rgb2cameranormal", "rgb2normal", "rgb2semantic", "rgb2pose"],
		["cameranormal2pose", "normal2pose", "semantic2pose"]
	],
	"four_paths" : [
		["rgb2cameranormal", "rgb2normal", "rgb2semantic", "rgb2halftone", "rgb2pose"],
		["cameranormal2pose", "normal2pose", "semantic2pose", "halftone2pose"]
	],
	"five_paths" : [
		["rgb2cameranormal", "rgb2normal", "rgb2semantic", "rgb2halftone", "rgb2wireframe", "rgb2pose"],
		["cameranormal2pose", "normal2pose", "semantic2pose", "halftone2pose", "wireframe2pose"]
	],
	"six_paths" : [
		["rgb2cameranormal", "rgb2normal", "rgb2semantic", "rgb2halftone", "rgb2wireframe", "rgb2depth", "rgb2pose"],
		["cameranormal2pose", "normal2pose", "semantic2pose", "halftone2pose", "wireframe2pose", "depth2pose"]
	],
	"seven_paths" : [
		["rgb2cameranormal", "rgb2normal", "rgb2semantic", "rgb2halftone", "rgb2wireframe", "rgb2depth", "rgb2opticalflow", "rgb2pose"],
		["cameranormal2pose", "normal2pose", "semantic2pose", "halftone2pose", "wireframe2pose", "depth2pose", "opticalflow2pose"]
	],
}

def rgb2pose_graph_ensemble_iter1_one_path(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["one_path"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2pose_graph_ensemble_iter1_two_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["two_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2pose_graph_ensemble_iter1_three_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["three_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2pose_graph_ensemble_iter1_four_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["four_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2pose_graph_ensemble_iter1_five_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["five_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2pose_graph_ensemble_iter1_six_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["six_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2pose_graph_ensemble_iter1_seven_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["seven_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)
