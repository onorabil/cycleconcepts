import torch as tr
import numpy as np
from functools import partial
from typing import List, Dict, Any
from neural_wrappers.pytorch import npGetData, trGetData, trModuleWrapper
from neural_wrappers.graph import Node, Edge, ConcatenateNode, forwardUseGT, MapNode, \
	Graph, forwardUseIntermediateResult, ForwardMessagesEdge, ReduceNode

from cycleconcepts.nodes import RGB, Depth, Semantic, \
	Normal, Wireframe, Halftone, CameraNormal, OpticalFlow, RGBNeighbour, Pose
from cycleconcepts.models import EncoderMap2Map

class NodeManager():
	nodes = {}

	@staticmethod
	def rgbPlusNeighbour(hyperParameters):
		rgbNode = NodeManager.getNode("rgb", hyperParameters)
		rgbPlusNeighbourNode = ConcatenateNode([rgbNode, RGBNeighbour(skip=hyperParameters["rgbNeighbourSkip"])], \
			concatenateFn=lambda x : tr.cat(list(x.values()), dim=-1), nodeEncoder=EncoderMap2Map(dIn=6))
		return rgbPlusNeighbourNode

	@staticmethod
	def getNode(Str, hyperParameters):
		if Node.lastNodeID == 0:
			NodeManager.nodes = {}

		possibleNodes = {
			"rgb" : (lambda : RGB()),
			"depth" : (lambda : Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])),
			"normal" : (lambda : Normal()),
			"cameranormal" : (lambda : CameraNormal()),
			"halftone" : (lambda : Halftone()),
			"opticalflow" : (lambda : OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], \
				opticalFlowMode=hyperParameters["opticalFlowMode"], \
				opticalFlowSkip=hyperParameters["opticalFlowSkip"])),
			"semantic" : (lambda : Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])),
			"pose" : (lambda : Pose(positionsExtremes=hyperParameters["positionsExtremes"], \
				poseRepresentation=hyperParameters["poseRepresentation"], \
				outResolution=hyperParameters["resolution"])),
			"rgbPlusNeighbour" : (lambda : NodeManager.rgbPlusNeighbour(hyperParameters)),
			"wireframe" : (lambda : Wireframe()),
			"poseDot" : (lambda : PoseDot(positionsExtremes=hyperParameters["positionsExtremes"], \
				poseRepresentation="translationOnly", outResolution=hyperParameters["resolution"], \
				dotRadius=hyperParameters["dotRadius"]))
		}
		if not Str in NodeManager.nodes:
			NodeManager.nodes[Str] = possibleNodes[Str]()
		return NodeManager.nodes[Str]

class EdgeManager():
	def __init__(self):
		self.edges = {}

	def getEdge(Str, hyperParameters, forwardFn, blockGradients, load=False):
		Str1, Str2 = Str.split("2")
		# rgb2opticalflow is a special case where we need the t+k neighbour for flow t -> t+k.
		# Thus, we can reuse that hyperparameter (we don't have other edges with neighbours so far,
		#  perhaps other project).
		if Str == "rgb2opticalflow":
			hyperParameters["rgbNeighbourSkip"] = hyperParameters["opticalFlowSkip"]
			node1 = NodeManager.getNode("rgbPlusNeighbour", hyperParameters)
		else:
			node1 = NodeManager.getNode(Str1, hyperParameters)
		node2 = NodeManager.getNode(Str2, hyperParameters)
		edge = Edge(node1, node2, forwardFn=forwardFn, blockGradients=blockGradients)

		if load:
			edge.loadPretrainedEdge("%s/%s/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"], Str))
		return edge

def forwardFnFineTune(self : Edge, x : tr.Tensor) -> tr.Tensor:
	# Output of the link we're optimizing
	thisGT = self.inputNode.getGroundTruth()

	# Compute the intermediate item
	ySingleLinksConcatenated = []
	messages = self.inputNode.messages
	for k in messages:
		if k == "GT":
			continue
		if self.residual == False and k == self.originalSingleLink:
			continue
		ySingleLinksConcatenated.append(messages[k])
	ySingleLinksConcatenated = tr.cat(ySingleLinksConcatenated).detach()
	yOrig = messages[self.originalSingleLink].squeeze(dim=0).detach()
	C = self.algorithm.getChoices(ySingleLinksConcatenated, yOrig, thisGT).detach()
	M = self.algorithm.getMask(C)
	ySingleLinksMerged = self.algorithm.getMerged(ySingleLinksConcatenated)
	yBlended = None
	if not M is None:
		M = M.detach()
		yBlended = (M * ySingleLinksMerged + (~M) * yOrig).detach()

	self.graphOutputs = {
		"singleLinksConcatenated" : ySingleLinksConcatenated,
		"singleLinksMerged" : ySingleLinksMerged,
		"choices" : C,
		"mask" : M,
		"blended" : yBlended,
		"originalSingleLink" : yOrig,
	}

	# Need to make a list of this because the default edge loss fn goes through all the outputs,
	#  and this means it will pass each string key independently.
	return yBlended.unsqueeze(dim=0) if not yBlended is None else None

def buildSubgraph(firstEdgesNames : List[str], secondEdgesNames : List[str], hyperParameters : Dict[str, Any], \
	residual : bool = True) -> Graph:
	firstEdges = [EdgeManager.getEdge(k, hyperParameters, forwardUseGT, True, True) for k in firstEdgesNames]
	secondEdges = [EdgeManager.getEdge(k, hyperParameters, forwardUseIntermediateResult, True, True) \
		for k in secondEdgesNames]
	rgbNode = NodeManager.getNode("rgb", hyperParameters)
	lastNode = firstEdges[-1].outputNode

	reduceOriginalNode = ReduceNode(lastNode, forwardFn=forwardFnFineTune)
	reduceOriginalNode.originalSingleLink = firstEdges[-1]
	reduceOriginalNode.residual = residual

	subGraph = Graph([
		*firstEdges,
		*secondEdges,
		reduceOriginalNode
	])
	subGraph.reduceLink = reduceOriginalNode
	subGraph.reduceLinkKey = tuple()
	# If we do subGrpah.subGraph=subGraph, PyTorch registers it and will call .eval() infinitely.
	subGraph.getSubgraph = lambda : subGraph
	subGraph.setTrainableWeights(False)
	return subGraph.eval()

class FineTuneNode(Node):
	def __init__(self, node, reduceLink):
		self.node = node
		self.reduceLink = reduceLink
		name = "FineTune %s" % (node.__repr__())
		super().__init__(name, node.groundTruthKey, node.nodeEncoder, node.nodeDecoder)
		self.__class__.__bases__ = (type(node), *self.__class__.__bases__)

	def getMetrics(self):
		metrics = {}
		for metricName, metric in self.node.getMetrics().items():
			newMetricName = "%s (on GT)" % (metricName)
			metrics[newMetricName] = metric
		nodeCriterion = self.node.getCriterion()
		metrics["Loss (on GT)"] = lambda y, t, **k : nodeCriterion(y, t)
		return metrics

	def getCriterion(self):
		return lambda y, t : ((y - self.reduceLink.graphOutputs["blended"])**2).mean()

def buildFineTune(subGraph : Graph, hyperParameters : Dict[str, Any]) -> Graph:
	rgbNode = NodeManager.getNode("rgb", hyperParameters)
	targetNode = subGraph.reduceLink.originalSingleLink.outputNode
	nodeName = targetNode.__repr__().lower()
	fineTuneLink = Edge(rgbNode, FineTuneNode(targetNode, subGraph.reduceLink), forwardFn=forwardUseGT)
	fineTuneLink.loadPretrainedEdge("%s/rgb2%s/model_best_Loss.pkl" % \
		(hyperParameters["singleLinksPath"], nodeName))
	fineTuneLink.setTrainableWeights(True)

	graph = Graph([
		subGraph,
		fineTuneLink
	])
	graph.reduceLink = subGraph.reduceLink
	graph.reduceLinkkKey = (subGraph, )
	graph.getSubgraph = lambda : subGraph
	return graph.eval()
