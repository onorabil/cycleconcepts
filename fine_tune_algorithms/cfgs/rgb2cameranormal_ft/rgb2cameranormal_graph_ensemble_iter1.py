from typing import Dict, Any
from neural_wrappers.graph import Graph
from neural_wrappers.graph import forwardUseGT, forwardUseIntermediateResult

from ..common import EdgeManager, buildSubgraph

cfgs = {
	"one_path" : [
		["rgb2normal", "rgb2cameranormal"],
		["normal2cameranormal"]
	],
	"two_paths" : [
		["rgb2wireframe", "rgb2normal", "rgb2cameranormal"],
		["wireframe2cameranormal", "normal2cameranormal"]
	],
	"three_paths" : [
		["rgb2semantic", "rgb2wireframe", "rgb2normal", "rgb2cameranormal"],
		["semantic2cameranormal","wireframe2cameranormal", "normal2cameranormal"]
	],
	"four_paths" : [
		["rgb2depth", "rgb2semantic", "rgb2wireframe", "rgb2normal", "rgb2cameranormal"],
		["depth2cameranormal","semantic2cameranormal","wireframe2cameranormal", "normal2cameranormal"]
	],
	"five_paths" : [
		["rgb2halftone", "rgb2depth", "rgb2semantic", "rgb2wireframe", "rgb2normal", "rgb2cameranormal"],
		["halftone2cameranormal", "depth2cameranormal","semantic2cameranormal","wireframe2cameranormal", "normal2cameranormal"]
	],
	"six_paths" : [
		["rgb2semantic", "rgb2normal", "rgb2depth", "rgb2halftone", "rgb2opticalflow", "rgb2wireframe", "rgb2cameranormal"],
		["semantic2cameranormal", "normal2cameranormal", "depth2cameranormal", "halftone2cameranormal", "opticalflow2cameranormal", "wireframe2cameranormal"]
	],
	"seven_paths" : [
		["rgb2semantic", "rgb2normal", "rgb2depth", "rgb2halftone", "rgb2opticalflow", "rgb2wireframe", "rgb2pose", "rgb2cameranormal"],
		["semantic2cameranormal", "normal2cameranormal", "depth2cameranormal", "halftone2cameranormal", "opticalflow2cameranormal", "wireframe2cameranormal"]
	]
}

def rgb2cameranormal_graph_ensemble_iter1_one_path(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["one_path"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2cameranormal_graph_ensemble_iter1_two_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["two_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2cameranormal_graph_ensemble_iter1_three_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["three_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2cameranormal_graph_ensemble_iter1_four_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["four_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2cameranormal_graph_ensemble_iter1_five_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["five_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2cameranormal_graph_ensemble_iter1_six_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["six_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2cameranormal_graph_ensemble_iter1_seven_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["seven_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)
