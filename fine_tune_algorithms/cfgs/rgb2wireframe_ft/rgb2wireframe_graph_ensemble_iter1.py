from typing import Dict, Any
from neural_wrappers.graph import Graph
from neural_wrappers.graph import forwardUseGT, forwardUseIntermediateResult

from ..common import EdgeManager, buildSubgraph

cfgs = {
	"one_path" : [
		["rgb2halftone", "rgb2wireframe"],
		["halftone2wireframe"]
	],
	"two_paths" : [
		["rgb2halftone", "rgb2cameranormal", "rgb2wireframe"],
		["halftone2wireframe", "cameranormal2wireframe"]
	],
	"three_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2cameranormal", "rgb2wireframe"],
		["halftone2wireframe", "normal2wireframe", "cameranormal2wireframe"]
	],
	"four_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2semantic", "rgb2cameranormal", "rgb2wireframe"],
		["halftone2wireframe", "normal2wireframe", "semantic2wireframe", "cameranormal2wireframe"]
	],
	"five_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2semantic", "rgb2cameranormal", "rgb2depth", "rgb2wireframe"],
		["halftone2wireframe", "normal2wireframe", "semantic2wireframe", "cameranormal2wireframe", "depth2wireframe"]
	]
}
def rgb2wireframe_graph_ensemble_iter1_one_path(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["one_path"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2wireframe_graph_ensemble_iter1_two_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["two_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2wireframe_graph_ensemble_iter1_three_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["three_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2wireframe_graph_ensemble_iter1_four_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["four_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2wireframe_graph_ensemble_iter1_five_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["five_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)
