import torch as tr
from typing import Dict, Any
from neural_wrappers.graph import forwardUseGT, Edge, Graph, ReduceNode
from cycleconcepts.nodes import RGB, Depth

def forwardFn(self : Edge, x : tr.Tensor) -> tr.Tensor:
	# Output of the link we're optimizing
	thisGT = self.inputNode.getGroundTruth()

	# Compute the intermediate item
	ySingleLinksConcatenated = []
	messages = self.inputNode.messages
	for k in messages:
		if k == "GT":
			continue
		if self.residual == False and k == self.originalSingleLink:
			continue
		ySingleLinksConcatenated.append(messages[k])
	ySingleLinksConcatenated = tr.cat(ySingleLinksConcatenated).detach()
	ySingleLinksMerged = ySingleLinksConcatenated.mean(dim=1).detach()
	yOrig = messages[self.originalSingleLink].squeeze(dim=0).detach()
	C = ySingleLinksMerged * 0
	M = ySingleLinksMerged * 0
	yBlended = ySingleLinksMerged

	self.graphOutputs = {
		"singleLinksConcatenated" : ySingleLinksConcatenated,
		"singleLinksMerged" : ySingleLinksMerged,
		"choices" : C,
		"mask" : M,
		"blended" : yBlended,
		"originalSingleLink" : yOrig,
	}

	# Need to make a list of this because the default edge loss fn goes through all the outputs,
	#  and this means it will pass each string key independently.
	return yBlended.unsqueeze(dim=0)

def rgb2depth_ensemble(hyperParameters : Dict[str, Any]) -> Graph:
	rgb = RGB()
	depth = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	depth_1 = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	depth_2 = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	depth_3 = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	depth_4 = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2Depth = Edge(rgb, depth, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Depth_1 = Edge(rgb, depth_1, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Depth_2 = Edge(rgb, depth_2, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Depth_3 = Edge(rgb, depth_3, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Depth_4 = Edge(rgb, depth_4, forwardFn=forwardUseGT, blockGradients=True)

	rgb2Depth.loadPretrainedEdge("%s/rgb2depth/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))
	rgb2Depth_1.loadPretrainedEdge("%s/rgb2depth_1/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))
	rgb2Depth_2.loadPretrainedEdge("%s/rgb2depth_2/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))
	rgb2Depth_3.loadPretrainedEdge("%s/rgb2depth_3/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))
	rgb2Depth_4.loadPretrainedEdge("%s/rgb2depth_4/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))

	lastNode = Depth
	reduceOriginalNode = ReduceNode(depth, forwardFn=forwardFn)

	graph = Graph([
		rgb2Depth,
		rgb2Depth_1,
		rgb2Depth_2,
		rgb2Depth_3,
		rgb2Depth_4,
		reduceOriginalNode
	])
	graph.reduceLink = reduceOriginalNode
	graph.reduceLinkKey = tuple()
	graph.getSubgraph = lambda : graph
	reduceOriginalNode.residual = True
	reduceOriginalNode.originalSingleLink = rgb2Depth

	return graph.eval()
