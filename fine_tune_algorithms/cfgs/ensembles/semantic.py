import torch as tr
from typing import Dict, Any
from neural_wrappers.graph import forwardUseGT, Edge, Graph, ReduceNode
from cycleconcepts.nodes import RGB, Semantic
from ..common import forwardFnFineTune

def rgb2semantic_ensemble(hyperParameters : Dict[str, Any]) -> Graph:
	rgb = RGB()
	semantic = Semantic(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semantic_1 = Semantic(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semantic_2 = Semantic(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2Semantic = Edge(rgb, semantic, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Semantic_1 = Edge(rgb, semantic_1, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Semantic_2 = Edge(rgb, semantic_2, forwardFn=forwardUseGT, blockGradients=True)

	rgb2Semantic.loadPretrainedEdge("%s/rgb2semantic/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))
	rgb2Semantic_1.loadPretrainedEdge("%s/rgb2semantic_1/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))
	rgb2Semantic_2.loadPretrainedEdge("%s/rgb2semantic_2/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"]))

	lastNode = Semantic
	reduceOriginalNode = ReduceNode(semantic, forwardFn=forwardFnFineTune)

	graph = Graph([
		rgb2Semantic,
		rgb2Semantic_1,
		rgb2Semantic_2,
		reduceOriginalNode
	])
	graph.reduceLink = reduceOriginalNode
	graph.reduceLinkKey = tuple()
	graph.getSubgraph = lambda : graph
	reduceOriginalNode.residual = True
	reduceOriginalNode.originalSingleLink = rgb2Semantic

	return graph.eval()
