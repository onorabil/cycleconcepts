from .depth import rgb2depth_ensemble
from .semantic import rgb2semantic_ensemble