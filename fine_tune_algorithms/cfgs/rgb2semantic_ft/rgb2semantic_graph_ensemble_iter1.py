from typing import Dict, Any
from neural_wrappers.graph import Graph
from neural_wrappers.graph import forwardUseGT, forwardUseIntermediateResult

from ..common import EdgeManager, buildSubgraph

cfgs = {
	"one_path" : [
		["rgb2halftone", "rgb2semantic"],
		["halftone2semantic"]
	],
	"two_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2semantic"],
		["halftone2semantic", "normal2semantic"]
	],
	"three_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2wireframe", "rgb2semantic"],
		["halftone2semantic", "normal2semantic", "wireframe2semantic"]
	],
	"four_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2wireframe", "rgb2cameranormal", "rgb2semantic"],
		["halftone2semantic", "normal2semantic", "wireframe2semantic", "cameranormal2semantic"]
	],
	"five_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2wireframe", "rgb2cameranormal", "rgb2depth", "rgb2semantic"],
		["halftone2semantic", "normal2semantic", "wireframe2semantic", "cameranormal2semantic", "depth2semantic"]
	],
	"six_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2wireframe", "rgb2cameranormal", "rgb2depth", \
			"rgb2opticalflow", "rgb2semantic"],
		["halftone2semantic", "normal2semantic", "wireframe2semantic", "cameranormal2semantic", \
		"depth2semantic", "opticalflow2semantic"]
	],
	"seven_paths" : [
		["rgb2halftone", "rgb2normal", "rgb2wireframe", "rgb2cameranormal", "rgb2depth", \
			"rgb2opticalflow", "rgb2pose", "rgb2semantic"],
		["halftone2semantic", "normal2semantic", "wireframe2semantic", "cameranormal2semantic", \
		"depth2semantic", "opticalflow2semantic", "pose2semantic"]
	]
}
def rgb2semantic_graph_ensemble_iter1_one_path(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["one_path"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2semantic_graph_ensemble_iter1_two_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["two_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2semantic_graph_ensemble_iter1_three_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["three_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2semantic_graph_ensemble_iter1_four_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["four_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2semantic_graph_ensemble_iter1_five_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["five_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2semantic_graph_ensemble_iter1_six_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["six_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)

def rgb2semantic_graph_ensemble_iter1_seven_paths(hyperParameters : Dict[str, Any]) -> Graph:
	firstEdges, secondEdges = cfgs["seven_paths"]
	return buildSubgraph(firstEdges, secondEdges, hyperParameters)
