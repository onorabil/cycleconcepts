import matplotlib.pyplot as plt
import numpy as np
import cv2
import transforms3d.euler as txe
from tqdm import tqdm

from neural_wrappers.utilities import getGenerators, npGetInfo, tryWriteImage, minMaxImage
from neural_wrappers.pytorch import npGetData, trGetData, npToTrCall

from cycleconcepts.plot_utils import toImageFuncs
from cycleconcepts.utils import makeNpy
from matplotlib.cm import viridis

def getChoiceImg(choice):
	choice = minMaxImage(choice)
	# choice = np.stack([choice * 255, choice * 255, choice * 255], axis=-1).astype(np.uint8)
	choice = (viridis(choice)[..., 0 : 3] * 255).astype(np.uint8)
	return choice

def getMaskImg(mask):
	return (np.stack([mask, mask, mask], axis=-1) * 255).astype(np.uint8)

def removeLast(x):
	return x[..., 0] if x.shape[-1] == 1 else x

def getDiffErrorImg(x):
	img = np.zeros((*x.shape, 3), dtype=np.uint8)
	# Errors lower than 10% of each image are considered equal.
	whereZeroError = np.abs(x) < 0.1
	whereBiggerError = x > 0
	whereSmallerError = x < 0
	img[whereBiggerError] = [0, 255, 0]
	img[whereSmallerError] = [255, 0, 0]
	img[whereZeroError] = [0, 0, 0]
	return img

def getDiffErrorClassification(reducedGraphValues, singleLinkValues, GT):
	reducedGraphValues = reducedGraphValues.argmax(dim=-1)
	singleLinkValues = singleLinkValues.argmax(dim=-1)
	GT = GT.argmax(dim=-1)

	# whereBothWrong = (reducedGraphValues != GT) & (singleLinkValues != GT)
	# whereBothRight = (reducedGraphValues == GT) & (singleLinkValues == GT)
	whereBetter = (reducedGraphValues == GT) & (singleLinkValues != GT)
	whereWorse = (reducedGraphValues != GT) & (singleLinkValues == GT)

	diffError = GT * 0
	diffError[whereBetter] = 1
	diffError[whereWorse] = -1
	return diffError

def makeImage(imgGT, imgSingleLink, imgBlended, imgDiffError, imgChoice, imgMask, imgOptimalMask):
	img = np.stack([imgGT, imgSingleLink, imgBlended, imgDiffError, imgChoice, imgMask, imgOptimalMask], axis=1)
	img = img.reshape((256, -1, 3))
	finalImg = np.zeros((300, *img.shape[1 : ]), dtype=np.uint8)
	finalImg[-256 : ] = img
	for i, text in enumerate(["GT", "Single Link", "Blended", "Diff Error", "Choice", "Mask", "Optimal Mask"]):
		w = 256 * i
		h = 30
		cv2.putText(finalImg, text, (w, h), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
	return finalImg

def saveImages(model, reader, batchSize, Type):
	targetNode = model.reduceLink.outputNode
	problemType = model.reduceLink.algorithm.problemType
	diffErrorFn = {
		"regression" : model.optimalChoice.getDiffError,
		"classification" : getDiffErrorClassification
	}[problemType]

	cnt = 0
	generator, numSteps = getGenerators(reader, batchSize, maxPrefetch=1, keys=["validation"])

	if Type == "export_video":
		writer = cv2.VideoWriter("out_clip.mp4", cv2.VideoWriter_fourcc(*"mp4v"), 15, (256*7, 300), True)

	for i in tqdm(range(numSteps)):
		data = next(generator)[0]
		model.iterationEpilogue(isTraining=False, isOptimizing=False, trLabels=trGetData(data))
		_ = model.npForward(data)
		graphOutput = model.reduceLink.graphOutputs

		thisGT = npGetData(targetNode.getGroundTruth())
		thisChoice = removeLast(graphOutput["choices"])
		thisConcatenated = graphOutput["singleLinksConcatenated"]
		thisBlended = npGetData(graphOutput["blended"])
		thisSingleLink = graphOutput["originalSingleLink"]
		thisMask = npGetData(removeLast(graphOutput["mask"]))
		thisOptimalChoice = npToTrCall(model.optimalChoice.getChoices, thisConcatenated, thisSingleLink, thisGT)
		thisOptimalMask = removeLast(model.optimalChoice.getMask(thisOptimalChoice))
		thisOptimalChoice = removeLast(thisOptimalChoice)

		thisDiffError = removeLast(npToTrCall(diffErrorFn, thisBlended, thisSingleLink, thisGT))
		MB = len(thisGT)

		# use toImageFuncs[type(node)](result, ...) to get formatted pictures
		thisSingleLink = npGetData(thisSingleLink)
		thisBlended = npGetData(thisBlended)
		thisChoice = npGetData(thisChoice)
		try:
			imgsSingleLink = toImageFuncs[type(targetNode)](thisSingleLink, thisGT, targetNode)
			imgsBlended = toImageFuncs[type(targetNode)](thisBlended, thisGT, targetNode)
			imgsGT = toImageFuncs[type(targetNode)](thisGT, thisGT, targetNode)
		except:
			pass # non-image output

		for j in range(MB):
			try:
				imgChoice = getChoiceImg(thisChoice[j])
				imgMask = getMaskImg(thisMask[j])
				imgOptimalChoice = getChoiceImg(thisOptimalChoice[j])
				imgOptimalMask = getMaskImg(thisOptimalMask[j])
				imgDiffError = getDiffErrorImg(thisDiffError[j])
				imgGT = imgsGT[j]
				imgSingleLink = imgsSingleLink[j]
				imgBlended = imgsBlended[j]
			except:
				pass # non-image output
			if Type == "export_images":
				img = makeImage(imgGT, imgSingleLink, imgBlended, imgDiffError, imgChoice, imgMask, imgOptimalMask)
				tryWriteImage(img, "plot_%d.png" % (cnt))
				if cnt >= 10:
					exit()
			elif Type == "export_video":
				img = makeImage(imgGT, imgSingleLink, imgBlended, imgDiffError, imgChoice, imgMask, imgOptimalMask)
				writer.write(img[..., ::-1])
			elif Type == "export_pseudolabels_npy":
				thisNpy = makeNpy(thisBlended[j], targetNode)
				np.save("%d.npy" % cnt, thisNpy)
			# elif Type == "export_custom":
			# 	np.savez_compressed("%d.npz" % cnt, thisBlended[j])
			# 	np.savez_compressed("%d_gt.npz" % cnt, thisGT[j].astype(np.uint8))
			# 	b=np.load("../../single_links_train/rgb2sema_sl/%d_gt.npz" % (cnt))["arr_0"].astype(np.int32)
			# 	assert (np.abs(b - thisGT[j])).sum() == 0

			cnt += 1
