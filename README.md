Repository for the NeurIPS paper. It contains 3 submodules:
- cycleconcepts => Definitions of nodes (RGB, Semantic, etc.)
- single_links_train => Definitions of cfgs (rgb2semantic, etc.) and everything related to training the single links
- fine_tune_using_variance => Definitions of the algorithms used to improve pre-trained single links

## Dataset information

The project works with multimodal datasets (RGB, Depth, Semantic etc.) and these must be exported in a specific format for things to work out of the box.

The format is simply:
```
{
	"train" : {
		"rgb" : array-of-rgbs,
		...
	},
	"validation" : {
		"rgb" : array-of-rgbs
	},
	"others" : {
		"dataStatistics" : {
			"whatever-statistics-need-to-be-stored" : array-of-statistics
		},
		(plus other stuff if required)
	}
}
```

Concretely, for `dataStatistics`, we store the minimum and maximum position (extracted from CARLA simulator), so we can normalize the `position` node properly and identically between train and test sets.

For paths datasets, we have a `baseDirectory` key in `others`, which represents the directory where each file is read from.

We support 3 types of datasets, all which follow the same format:

#### 1. H5 dataset

This stores the raw data (rgb, depth) as numpy arrays that are stored in the H5 file itself. Advantages of doing this is the speed of reading data, which might improve training time. However, this results in very large (because data is uncompressed) dataset size.

#### 2. Raw images + paths dataset

This stores the data as raw images (PNG format is used by us, but not obligatory) and a small H5 file that respects the format described above. However, instead of having an `array-of-rgbs`, it's simply the path to the file, starting from the base directory.

For example, if the first train rgb image is stored at `/path/to/datasetRoot/rgb/train/1.png`, then the first entry in the `{"train" : {"rgb" : paths}}` h5 array is `rgb/train/1.png` and `baseDirectory` in `others` is set to `/path/to/datasetRoot`.

#### 3. Npy images + paths dataset

This stores the data as raw npy files. Besides using NumPy instead of OpenCV/lycon/scikit-image to read images, everything is identical. This format is mostly used, as we can store raw data (so we don't hack depth images as 24-bit PNG format and such), as well as being able to use a resized version of the images if desired (so we don't resize every time we read).

Regarding space vs speed, this is the best approach and is used for the project.

The reader class in `cycleconcepts/reader.py` will detect automatically what type of reader is used from the h5 metadata and will read the information accordingly.
