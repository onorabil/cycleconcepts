#!/bin/bash
# Usage: ./update_pseudolabels <original_test_set> <resulting_test_set> <generated_test_set> <key_to_replace>
test $# -eq 4 || echo "./update_pseudolabels.sh <original_test_set> <resulting_test_set> <generated_test_set> <key_to_replace>"
test $# -eq 4 || exit 1

echo "Overwriting use case '$4' in '$2' using names from '$1' and data from '$3'"
read -p "Press enter to continue"

# Step 1: We need the "original" test set based on which the "generated" test set is created. This is the first directory
rm "$4".txt
i=$(ls $1 | grep _$4_ | grep npy | wc -l)
echo "Found $i paths"
test $i -gt 1 || echo "No paths found"
test $i -gt 1 || exit 1
read -p "Press enter to continue"
ls $1 | grep _$4_ | grep npy | sort -n | while read line; do
	echo $line >> "$4".txt;
done

read -p "Written file. Press enter to continue"

# Step 2: Check that we have the same amount of paths _where_ we want to store and _from where_ we want to take data
j=$(ls $3 | grep npy | wc -l)
test $i -eq $j || "In $3 there are $j items, while in $1 there are $i"
test $i -eq $j || exit 1

cat "$4.txt" | while read line; do
	test -e "$2/$line" || echo "$2/$line does not exist"
	test -e "$2/$line" || exit 1
done

# Step 3: Do the actual changes
i=0
cat "$4.txt" | while read line; do
	echo "$i/$j"
	a="`realpath $3`/$i.npy"
	b="`realpath $2`/$line"
	unlink $b
	ln -s $a $b
	i=$(($i+1))
done
