import os
import numpy as np
from datetime import datetime
from argparse import ArgumentParser
from tqdm import tqdm
from pathlib import Path
from neural_wrappers.utilities import tryReadImage, npGetInfo, npCloseEnough, resize, tryWriteImage
from neural_wrappers.readers.datasets.carla.utils import unrealPngFromFloat, unrealFloatFromPng

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("baseDirectory")
	parser.add_argument("resultDirectory")
	parser.add_argument("mode")
	parser.add_argument("--desiredShape", default="256,256")
	parser.add_argument("--batchSize", default=100, type=int)
	args = parser.parse_args()

	assert args.mode in ("images", "npy")
	args.desiredShape = args.desiredShape.split(",")
	args.baseDirectory = os.path.abspath(args.baseDirectory)
	args.resultDirectory = os.path.abspath(args.resultDirectory)
	return args

def doResizeNpy(img, inPath, height, width):
	if "semantic" in inPath:
		imgResized = resize(img, width=width, height=height, interpolation="nearest", resizeLib="opencv")
	elif ("depth" in inPath) or ("flow" in inPath):
		decodedImg = unrealFloatFromPng(img)
		imgResized = resize(decodedImg, width=width, height=height, interpolation="bilinear", resizeLib="skimage")
	else:
		imgResized = resize(img, width=width, height=height, interpolation="bilinear", resizeLib="skimage")
	return imgResized

def doResizePng(img, inPath, height, width):
	if "semantic" in inPath:
		imgResized = resize(img, width=width, height=height, interpolation="nearest", resizeLib="opencv")
	elif ("depth" in inPath) or ("flow" in inPath):
		decodedImg = unrealFloatFromPng(img)
		imgResized = resize(decodedImg, width=width, height=height, interpolation="bilinear", resizeLib="skimage")
		imgResized = unrealPngFromFloat(imgResized)
	else:
		imgResized = resize(img, width=width, height=height, interpolation="bilinear", resizeLib="skimage")
	return imgResized

def doBatch(args, thisFiles):
	inPaths = list(map(lambda x : "%s/%s" % (args.baseDirectory, x), thisFiles))
	if args.mode == "images":
		outPaths = list(map(lambda x : "%s/%s" % (args.resultDirectory, x), thisFiles))
		doResizeFn = doResizePng
		saveFn = lambda path, file : tryWriteImage(file, path, imgLib="lycon")
	elif args.mode == "npy":
		outPaths = list(map(lambda x : "%s/%s" % (args.resultDirectory, x.replace("png", "npy")), thisFiles))
		doResizeFn = doResizeNpy
		saveFn = np.save

	imgs = [tryReadImage(inPath) for inPath in inPaths]

	results = []
	for img, path in zip(imgs, inPaths):
		results.append(doResizeFn(img, path, height=args.desiredShape[0], width=args.desiredShape[1]))

	for result, path in zip(results, outPaths):
		saveFn(path, result)

def main():
	args = getArgs()
	files = list(filter(lambda x : x.endswith(".png"), map(lambda x : x.name, os.scandir(args.baseDirectory))))
	# files = list(filter(lambda x : (x.find("depth") != -1) or (x.find("flow") != -1), files))
	# files = list(filter(lambda x : x.find("semantic") != -1, files))
	# files = list(filter(lambda x : (x.find("flowx1") != -1) or (x.find("flowy1") != -1), files))
	print("Found %d files to be converted" % len(files))
	Path(args.resultDirectory).mkdir(parents=True, exist_ok=True)

	N = len(files) // args.batchSize + (len(files) % args.batchSize != 0)
	for i in tqdm(range(N)):
		startIndex, endIndex = i * args.batchSize, min((i + 1) * args.batchSize, len(files))
		thisFiles = files[startIndex : endIndex]

		doBatch(args, thisFiles)

if __name__ == "__main__":
	main()
