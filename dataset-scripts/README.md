Dataset process

This module holds the scripts to generate the original train_set as well as semisupervised train sets and iteration 1, 2,... train_sets based on pseudolabels.

TODO: Process in a more elaborate way.

Step 1: Export RGB for new flight

Step 2: Dataset resize and export as NPY (dataset_resize.py) -> semisupervised_set_N_256_npy dir

Step 3: ... Create dummy NPY for all representations (generate_dummy_npy.py) ...

Step 4: Call carla_h5_converter -> generate paths h5 -> semisupervised_set_N_256.npy

Step 5: Merge pose (merge_pose.py)

Step 6: merge iteration datasets: iter n-1 dataset + new dataset (merge_iteration_datasets.py) -> train_set_iterationN_256.h5

Step 7: For each representation, call export_pseudolabels_npy for G-E or export_npy for single links:
`CUDA_VISIBLE_DEVICES=2 python main.py export_pseudolabels_npy rgb2depth_graph_ensemble_best /scratch/nvme0n1/Datasets/Carla/final_dataset/semisupervised_set_N_256.h5 /scratch/nvme0n1/Datasets/Carla/final_dataset/train_set_256.h5 --resolution=256,256 --singleLinksPath=single_links_bank/iteration_N/depth/nogt/ --algorithm=reduce`

Step 8: `mv pseudolabels_depth_iter2/ /scratch/nvme0n1/Datasets/Carla/final_dataset/pseudolabels_N/generated_depth`

Step 9: `bash /scratch/nvme0n1/graph_project/dataset-scripts/update_pseudupervised_set_N_256_npy/ semisupervised_set_N_256_npy/ pseudolabels_N/generated_depth/ depth`

Step 10: Train rgb2depth using train_set_iterationN_256.h5
`CUDA_VISIBLE_DEVICES=3 python3.8 main.py train rgb2depth /scratch/nvme0n1/Datasets/Carla/final_dataset/train_set_iterationN_256.h5 --resolution=256,256 --batch_size=20 --dir=rgb2depth_dslN --lr=0.004 --factor=2 --patience=10 --maxDepthMeters=300`