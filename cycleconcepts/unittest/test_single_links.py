import sys
sys.path.append("..")
sys.path.append("../cfgs")
import single_link
from inspect import getmembers, isfunction
from neural_wrappers.graph import Graph

class FakeArgs:
	def __init__(self, x):
		self.x = x

	def __getattr__(self, name):
		return self.x[name]

args = FakeArgs({
	"positionsExtremes" : {"mins" : [0, 0, 0, 0, 0, 0], "maxs" : [0, 0, 0, 0, 0, 0]},
	"max_depth_meters" : 300,
	"resolution" : (256, 256)
})

class TestSingleLinks:
	def test_save_weights_1(self):
		functions_list = [o for o in getmembers(single_link) if isfunction(o[1])]
		for name, func in functions_list:
			res = func(args)
			assert type(res) is Graph
