import numpy as np
import torch.nn as nn
from neural_wrappers.pytorch import NeuralNetworkPyTorch
from neural_wrappers.pytorch.utils import getNumParams
import torch as tr


def conv(dIn, dOut, kernel_size, padding=None, stride=2, dilation=1):
	if padding == None:
		padding = (kernel_size-1)//2
	return nn.Sequential(
		nn.Conv2d(in_channels=dIn, out_channels=dOut, kernel_size=kernel_size, padding=padding, \
			stride=stride, dilation=dilation),
		nn.BatchNorm2d(num_features=dOut),
		nn.ReLU(inplace=True)
	)


# Generic network that computes "correlation features" between two RGB frames, concatenating them by the last channel
class EncoderMap2VectorOriginal(NeuralNetworkPyTorch):
	def __init__(self, dIn, numFilters=256):
		super().__init__()
		self.dIn = dIn
		self.numFilters = numFilters

		conv_planes = [16, 32, 64, 128, 256, 256]
		self.conv1 = conv(dIn=dIn, dOut=conv_planes[0], kernel_size=7)
		self.conv2 = conv(dIn=conv_planes[0], dOut=conv_planes[1], kernel_size=5)
		self.conv3 = conv(dIn=conv_planes[1], dOut=conv_planes[2], kernel_size=3)
		self.conv4 = conv(dIn=conv_planes[2], dOut=conv_planes[3], kernel_size=3)
		self.conv5 = conv(dIn=conv_planes[3], dOut=conv_planes[4], kernel_size=3)
		self.conv6 = conv(dIn=conv_planes[4], dOut=conv_planes[5], kernel_size=3)
		self.conv7 = conv(dIn=conv_planes[5], dOut=numFilters, kernel_size=3)


	def forward(self, x):
		x = x.transpose(1, 3).transpose(2, 3)
		out_conv1 = self.conv1(x)
		out_conv2 = self.conv2(out_conv1)
		out_conv3 = self.conv3(out_conv2)
		out_conv4 = self.conv4(out_conv3)
		out_conv5 = self.conv5(out_conv4)
		out_conv6 = self.conv6(out_conv5)
		out_conv7 = self.conv7(out_conv6)
		return out_conv7

	def __str__(self):
		return "Encoder Map2Vector. dIn: %d. NF: %d Params: %d)" % \
			(self.dIn, self.dOut, getNumParams(self.parameters())[1])

class EncoderMap2Vector(NeuralNetworkPyTorch):
	def __init__(self, dIn, numFilters=16):
		super().__init__()
		self.dIn = dIn
		self.numFilters = numFilters

		self.conv1 = conv(dIn=dIn, dOut=numFilters, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv2 = conv(dIn=numFilters, dOut=numFilters, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv3 = conv(dIn=numFilters, dOut=numFilters, kernel_size=3, padding=1, stride=2, dilation=1)

		self.conv4 = conv(dIn=numFilters, dOut=numFilters * 2, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv5 = conv(dIn=numFilters * 2, dOut=numFilters * 2, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv6 = conv(dIn=numFilters * 2, dOut=numFilters * 2, kernel_size=3, padding=1, stride=2, dilation=1)

		self.conv7 = conv(dIn=numFilters * 2, dOut=numFilters * 4, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv8 = conv(dIn=numFilters * 4, dOut=numFilters * 4, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv9 = conv(dIn=numFilters * 4, dOut=numFilters * 4, kernel_size=3, padding=1, stride=2, dilation=1)

		self.dilate1 = conv(dIn=numFilters * 4, dOut=numFilters * 8, kernel_size=3, padding=1, stride=1, dilation=1)
		self.dilate2 = conv(dIn=numFilters * 8, dOut=numFilters * 8, kernel_size=3, padding=2, stride=1, dilation=2)
		self.dilate3 = conv(dIn=numFilters * 8, dOut=numFilters * 8, kernel_size=3, padding=4, stride=1, dilation=4)
		self.dilate4 = conv(dIn=numFilters * 8, dOut=numFilters * 8, kernel_size=3, padding=8, stride=1, dilation=8)
		self.dilate5 = conv(dIn=numFilters * 8, dOut=numFilters * 8, kernel_size=3, padding=16, stride=1, dilation=16)
		self.dilate6 = conv(dIn=numFilters * 8, dOut=numFilters * 8, kernel_size=3, padding=32, stride=1, dilation=32)
		
		self.conv10 = conv(dIn=numFilters * 8, dOut=numFilters * 4, kernel_size=3, padding=1, stride=2, dilation=1)
		self.conv11 = conv(dIn=numFilters * 4, dOut=numFilters * 4, kernel_size=3, padding=1, stride=1, dilation=1)

		self.conv12 = conv(dIn=numFilters * 4, dOut=numFilters * 2, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv13 = conv(dIn=numFilters * 2, dOut=numFilters * 2, kernel_size=3, padding=1, stride=2, dilation=1)

		self.conv14 = conv(dIn=numFilters *2, dOut=numFilters, kernel_size=3, padding=1, stride=1, dilation=1)
		self.conv15 = conv(dIn=numFilters, dOut=numFilters, kernel_size=3, padding=1, stride=2, dilation=1)

		

	def forward(self, x):
		# x::dInxHxW
		x = x.transpose(1, 3).transpose(2, 3)
		y_1 = self.conv1(x)
		y_2 = self.conv2(y_1)
		y_3 = self.conv3(y_2)
		y_4 = self.conv4(y_3)
		y_5 = self.conv5(y_4)
		y_6 = self.conv6(y_5)

		y_7 = self.conv7(y_6)
		y_8 = self.conv8(y_7)
		y_9 = self.conv9(y_8)

		y_dilate1 = self.dilate1(y_9)
		y_dilate2 = self.dilate2(y_dilate1)
		y_dilate3 = self.dilate3(y_dilate2)
		y_dilate4 = self.dilate4(y_dilate3)
		y_dilate5 = self.dilate5(y_dilate4)
		y_dilate6 = self.dilate6(y_dilate5)
		y_dilate_sum = y_dilate1 + y_dilate2 + y_dilate3 + y_dilate4 + y_dilate5 + y_dilate6

		y_10 = self.conv10(y_dilate_sum)
		y_11 = self.conv11(y_10)
		y_12 = self.conv12(y_11)
		y_13 = self.conv13(y_12)
		y_14 = self.conv14(y_13)
		y_15 = self.conv15(y_14)

		flattened = tr.flatten(y_15, start_dim=1, end_dim=-1)


		return flattened

	def __str__(self):
		return "Encoder Map2Vector. dIn: %d. NF: %d. Params: %d)" % (self.dIn, \
			self.numFilters, getNumParams(self.parameters())[1])

class DecoderMap2VectorOriginal(NeuralNetworkPyTorch):
	def __init__(self, dOut, numFilters=256):
		super().__init__()
		self.dOut = dOut
		self.numFilters = numFilters
		self.decoder1 = nn.Linear(in_features=numFilters * 4, out_features=numFilters)
		self.decoder2 = nn.Linear(in_features=numFilters, out_features=numFilters)
		self.decoder3 = nn.Linear(in_features=numFilters, out_features=dOut)

	def forward(self, x):
		x = x.reshape(x.shape[0], -1)
		y1 = self.decoder1(x)
		y2 = self.decoder2(y1)
		y3 = self.decoder3(y2)

		return y3

	def __str__(self):
		return "Decoder Map2Vector. dOut: %d. NF: %d Params: %d)" % \
			(self.dOut, self.dOut, getNumParams(self.parameters())[1])


class DecoderMap2Vector(NeuralNetworkPyTorch):
	def __init__(self, dOut, numFilters=16, outClasses=6):
		super().__init__()
		self.dOut = dOut
		self.numFilters = numFilters
		self.decoder1 = nn.Linear(in_features=numFilters *16, out_features=numFilters)
		self.decoder2 = nn.Linear(in_features=numFilters, out_features=numFilters)
		self.decoder3 = nn.Linear(in_features=numFilters, out_features=outClasses)

	def forward(self, x):
		y1 = self.decoder1(x)
		y2 = self.decoder2(y1)
		y3 = self.decoder3(y2)

		return y3

	def __str__(self):
		return "Decoder Map2Vector. dOut: %d. NF: %d Params: %d)" % \
			(self.dOut, self.dOut, getNumParams(self.parameters())[1])


class DecoderMap2VectorCombined(NeuralNetworkPyTorch):
	def __init__(self, dOut, numFilters=16, outClasses=6):
		super().__init__()
		self.dOut = dOut
		self.numFilters = numFilters
		self.decoder1 = nn.Linear(in_features=numFilters *16, out_features=numFilters)
		self.decoder2 = nn.Linear(in_features=numFilters, out_features=numFilters)
		self.decoder3 = nn.Linear(in_features=numFilters, out_features=3)

		self.decoder1_0 = nn.Linear(in_features=numFilters *16, out_features=numFilters)
		self.decoder2_0 = nn.Linear(in_features=numFilters, out_features=numFilters)
		self.decoder3_0 = nn.Linear(in_features=numFilters, out_features=6)


	def forward(self, x):
		y1 = self.decoder1(x)
		y2 = self.decoder2(y1)
		y3 = self.decoder3(y2)

		y1_0 = self.decoder1_0(x)
		y2_0 = self.decoder2_0(y1_0)
		y3_0 = self.decoder3_0(y2_0)

		y_final = tr.cat((y3, y3_0), dim=1)

		return y_final

	def __str__(self):
		return "Decoder Map2Vector. dOut: %d. NF: %d Params: %d)" % \
			(self.dOut, self.dOut, getNumParams(self.parameters())[1])
