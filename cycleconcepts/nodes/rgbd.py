import torch as tr
from neural_wrappers.graph import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO
from functools import partial

from ..models import EncoderMap2Vector, EncoderMap2Map, DecoderVector2Map, DecoderMap2Map
from .rgb import RGB
from .pose import Pose
from .depth import Depth

class EncoderRGBDMap2Vector(EncoderMap2Vector):
	def __init__(self, numFilters=256):
		super().__init__(dIn=4, numFilters=numFilters)

	def forward(self, x):
		x = tr.cat([x["rgb"], x["depth"].unsqueeze(dim=-1)], dim=-1)
		return super().forward(x)

class EncoderRGBDMa2pMap(EncoderMap2Map):
	pass

class DecoderRGBDVector2Map(DecoderVector2Map):
	def __init__(self, numFilters=16):
		super().__init__(dOut=4, numFilters=numFilters)

	def forward(self, x):
		y = super().forward(x)
		y = {"rgb" : y[..., 0 : 3], "depth" : y[..., 3].unsqueeze(dim=-1)}
		return y

class DecoderRGBDMap2Map(DecoderMap2Map):
	pass

# Custom copound node made of rgb and depth
class RGBD(MapNode):
	def __init__(self, maxDepthMeters, hyperParameters={}):
		hyperParameters["maxDepthMeters"] = maxDepthMeters
		self.maxDepthMeters = maxDepthMeters
		super().__init__(name="RGBD", groundTruthKey=["rgb", "depth"], hyperParameters=hyperParameters)

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			VectorNode : EncoderRGBDMap2Vector,
			MapNode : EncoderRGBDMa2pMap
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			VectorNode : DecoderRGBDVector2Map,
			MapNode: DecoderRGBDMap2Map
		}
		return pickTypeFromMRO(inputNodeType, modelTypes)()

	def getMetrics(self):
		return {
			"RGB (L1 pixel)" : RGBD.RGBMetricL1Pixel,
			"RGB (L2)" : RGBD.RGBMetricL2,
			"Depth (m)" : partial(RGBD.depthMetric, maxDepthMeters=self.maxDepthMeters)
		}

	def getCriterion(self):
		return RGBD.lossFn

	def lossFn(y, t):
		rgbLoss = RGB.lossFn(y["rgb"], t["rgb"])
		depthLoss = Depth.lossFn(y["depth"], t["depth"])
		return rgbLoss + depthLoss

	def RGBMetricL1Pixel(y, t, **k):
		return RGB.RGBMetricL1Pixel(y["rgb"], t["rgb"], **k)

	def RGBMetricL2(y, t, **k):
		return RGB.RGBMetricL2(y["rgb"], t["rgb"], **k)

	def depthMetric(y, t, maxDepthMeters, **k):
		return Depth.depthMetric(y["depth"], t["depth"], maxDepthMeters, **k)
