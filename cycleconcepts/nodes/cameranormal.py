import numpy as np
import torch
import torch.nn.functional as F
from .normal import Normal, MapNode

class CameraNormal(Normal):
	def __init__(self):
		MapNode.__init__(self, name="CameraNormal", groundTruthKey="cameranormal")

	def getMetrics(self):
		return {
			"CameraNormal-v1 (deg)" : CameraNormal.degreeMetricV1,
			"CameraNormal-v2 (deg)" : CameraNormal.degreeMetricV2,
			"CameraNormal (deg)" : CameraNormal.degreeMetric,
		}

	@staticmethod
	def degreeMetricV1(y, t, **k):
		# First, remove sign from both and then do the L1 diff
		y = np.abs(y)
		t = np.abs(t)
		diff = np.abs(y - t)
		return diff.mean() * 360

	@staticmethod
	def degreeMetricV2(y, t, **k):
		# First, remove sign from both and then do the L1 diff
		y = np.abs(y)
		t = np.abs(t)
		t[...,2] = 1 - t[...,2]
		y[...,2] = 1 - y[...,2]

		y = torch.from_numpy(y)
		t = torch.from_numpy(t)

		cosine_distance = F.cosine_similarity(y, t)
		cosine_distance = np.minimum(np.maximum(cosine_distance.cpu().numpy(), -1.0), 1.0)

		angles = np.arccos(cosine_distance) / np.pi * 180.0
		return angles.mean()

	@staticmethod
	def degreeMetric(y, t, **k):
		# First, remove sign from both and then do the L1 diff
		y = np.abs(y)
		t = np.abs(t)
		t[...,2] = 1 - t[...,2]
		y[...,2] = 1 - y[...,2]

		y = torch.from_numpy(y)
		t = torch.from_numpy(t)

		cosine_distance = F.cosine_similarity(y, t, dim=-1)
		cosine_distance = cosine_distance.cpu().numpy()
		cosine_distance = np.abs(cosine_distance)

		angles = np.arccos(cosine_distance) / np.pi * 180
		angles[np.isnan(angles)] = 180
		return angles.mean()