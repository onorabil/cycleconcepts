import numpy as np
import torch as tr
from functools import partial
from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map
from neural_wrappers.graph.node import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO, toCategorical
from neural_wrappers.pytorch import trModuleWrapper, device
from neural_wrappers.metrics import MeanIoU, Accuracy, MetricWrapper

def fromBinaryToMulticlass(results, labels, **kwargs):
	y = np.concatenate([1 - results, results], axis=-1).reshape(-1, 2)
	t = toCategorical(labels.astype(np.uint8), numClasses=2).astype(np.float32)
	return y, t, kwargs

class Wireframe(MapNode):
	def __init__(self):
		super().__init__(name="Wireframe", groundTruthKey="wireframe")

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			MapNode : partial(EncoderMap2Map, dIn=1),
			VectorNode : partial(EncoderMap2Vector, dIn=1),
			# type(None) : ModelRGB # TODO
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Map, dOut=1),
			VectorNode : partial(DecoderVector2Map, dOut=1)
		}
		model = pickTypeFromMRO(inputNodeType, modelTypes)().to(device)
		return trModuleWrapper(lambda x : tr.sigmoid(model(x)))

	def getMetrics(self):
		return {
			"Mean IoU (global)" : MetricWrapper(fromBinaryToMulticlass, MeanIoU(mode="global")),
			"Accuracy" : MetricWrapper(fromBinaryToMulticlass, Accuracy())
		}

	def getCriterion(self):
		return Wireframe.lossFn

	def lossFn(y, t):
		L = -(t * tr.log(y + 1e-5) + (1 - t) * tr.log(1 - y + 1e-5))
		return L.mean()
