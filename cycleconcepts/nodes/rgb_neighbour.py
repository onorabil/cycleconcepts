from .rgb import RGB
from neural_wrappers.graph.node import MapNode

class RGBNeighbour(RGB):
	def __init__(self, skip : int):
		self.skip = skip
		MapNode.__init__(self, name="RGBNeighbour(t+%d)" % (skip), groundTruthKey="rgbNeighbour(t+%d)" % (skip))
