import numpy as np
from neural_wrappers.graph import ConcatenateNode, MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO
from functools import partial
from .rgb import RGB
from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map

class OpticalFlow(RGB):
	def __init__(self, opticalFlowPercentage, opticalFlowMode, opticalFlowSkip):
		self.opticalFlowPercentage = opticalFlowPercentage
		self.opticalFlowMode = opticalFlowMode
		self.opticalFlowSkip = opticalFlowSkip
		self.numDimensions = {
			"xy" : 2,
			"magnitude" : 1,
			"xy_plus_magnitude" : 3
		}[self.opticalFlowMode]

		MapNode.__init__(self, name="OpticalFlow(t+%d)" % (opticalFlowSkip), \
			groundTruthKey="optical_flow(t+%d)" % (opticalFlowSkip))

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			MapNode : partial(EncoderMap2Map, dIn=self.numDimensions),
			VectorNode : partial(EncoderMap2Vector, dIn=self.numDimensions),
			# type(None) : ModelRGB # TODO
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Map, dOut=self.numDimensions),
			VectorNode : partial(DecoderVector2Map, dOut=self.numDimensions)
		}
		return pickTypeFromMRO(inputNodeType, modelTypes)()

	def getMetrics(self):
		metrics = {
			"Optical Flow (L2)" : RGB.RGBMetricL2
		}
		if self.opticalFlowMode in ("xy", "xy_plus_magnitude"):
			metrics["Optical Flow (L1 pixel)"] = partial(OpticalFlow.FlowMetricL1Pixel, \
				opticalFlowPercentage=self.opticalFlowPercentage)
		if self.opticalFlowMode in ("magnitude", "xy_plus_magnitude"):
			metrics["Optical Flow Magnitude (L1 pixel)"] = partial(OpticalFlow.FlowMetricMagnitudeL1Pixel, \
				opticalFlowPercentage=self.opticalFlowPercentage)
		return metrics

	@staticmethod
	def FlowMetricL1Pixel(y, t, opticalFlowPercentage, **k):
		y = np.clip(y[..., 0 : 2], 0, 1)
		t = t[..., 0 : 2]
		H, W = y.shape[1], y.shape[2]

		# Remap y and t from [0 : 1] to [-1 : 1]
		y = (y - 0.5) * 2
		t = (t - 0.5) * 2
		# y, t :: [-1:1] representing x%, y% of image. We need to remap to H, W (100%,100%)
		value = np.array(opticalFlowPercentage) * (W, H) / 100

		# MBxHxWx2
		y[..., 0] *= value[0]
		t[..., 0] *= value[0]
		y[..., 1] *= value[1]
		t[..., 1] *= value[1]
		return np.abs(y - t).mean()

	@staticmethod
	def FlowMetricMagnitudeL1Pixel(y, t, opticalFlowPercentage, **k):
		# Magnitude is last dimension always, be it the only one or 3rd
		y = np.clip(y[..., -1], 0, 1)
		t = t[..., -1]
		H, W = y.shape[1], y.shape[2]
		# Find the percentage (max pixel value) and compute the magnitude
		# Basically, if max is 10% of 256x180, we get 25.6x18, so the max magnitude is hypot(25.6, 18)
		value = np.array(opticalFlowPercentage) * (W, H) / 100
		value = np.hypot(value[0], value[1])
		return np.abs(y - t).mean() * value
