import numpy as np
import torch
import torch.nn.functional as F
from neural_wrappers.graph.node import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO
from functools import partial
from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map

class Normal(MapNode):
	def __init__(self):
		super().__init__(name="Normal", groundTruthKey="normal")

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			MapNode : partial(EncoderMap2Map, dIn=3),
			VectorNode : partial(EncoderMap2Vector, dIn=3),
			# type(None) : ModelNormal # TODO
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Map, dOut=3),
			VectorNode : partial(DecoderVector2Map, dOut=3)
		}
		return pickTypeFromMRO(inputNodeType, modelTypes)()

	def getMetrics(self):
		return {
			"Normal-v1 (deg)" : Normal.degreeMetricV1,
			"Normal-v2 (deg)" : Normal.degreeMetricV2,
			"Normal (deg)" : Normal.degreeMetric,
		}

	@staticmethod
	def degreeMetricV1(y, t, **k):
		# First, remove sign from both and then do the L1 diff
		y = np.abs(y)
		t = np.abs(t)
		diff = np.abs(y - t)
		return diff.mean() * 360

	@staticmethod
	def degreeMetricV2(y, t, **k):
		# First, remove sign from both and then do the L1 diff
		y = np.abs(y)
		t = np.abs(t)
		y = torch.from_numpy(y)
		t = torch.from_numpy(t)
		cosine_distance = F.cosine_similarity(y, t)
		cosine_distance = np.minimum(np.maximum(cosine_distance.cpu().numpy(), -1.0), 1.0)
		angles = np.arccos(cosine_distance) / np.pi * 180.0
		return angles.mean()

	@staticmethod
	def degreeMetric(y, t, **k):
		# First, remove sign from both and then do the L1 diff
		y = np.abs(y)
		t = np.abs(t)
		y = torch.from_numpy(y)
		t = torch.from_numpy(t)
		cosine_distance = F.cosine_similarity(y, t, dim=-1)
		cosine_distance = cosine_distance.cpu().numpy()
		cosine_distance = np.abs(cosine_distance)
		angles = np.arccos(cosine_distance) / np.pi * 180
		angles[np.isnan(angles)] = 180
		return angles.mean()

	def getCriterion(self):
		return Normal.lossFn

	@staticmethod
	def lossFn(y, t):
		return ((y - t)**2).mean()