import sys
sys.path.append("../")

import numpy as np
import torch as tr
import torch.optim as optim
from argparse import ArgumentParser
from neural_wrappers.utilities import changeDirectory, getGenerators
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetrics, Callback
from neural_wrappers.schedulers import ReduceLRAndBacktrackOnPlateau

from cycleconcepts.utils import getModelDataDimsAndHyperParams, fullPath, addNodesArguments
from cycleconcepts.reader import Reader
from cycleconcepts.nodes import *
from cycleconcepts.models import *

from cfgs import *
from test_export_images import test_export_images
from test_export_npz import test_export_npz
from test_export_video import test_export_video
from test_dataset import test_dataset

tr.backends.cudnn.deterministic = True
tr.backends.cudnn.benchmark = False

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("graph_config")
	parser.add_argument("dataset_path")

	# Models
	parser.add_argument("--weights_file")

	# Training stuff
	parser.add_argument("--batch_size", default=5, type=int)
	parser.add_argument("--num_epochs", default=100, type=int)
	parser.add_argument("--lr", default=0.001, type=float)
	parser.add_argument("--firstLinksPath", help="Used by two-hops links using pre-trained first hop.")
	parser.add_argument("--seed", default=0, type=int)
	parser.add_argument("--patience", default=0, type=int)
	parser.add_argument("--factor", type=float)
	parser.add_argument("--optimizer", default="adamw")

	# Reader stuff
	parser.add_argument("--resolution", default="512,512")

	# Various nodes hyperparameters
	parser = addNodesArguments(parser)

	parser.add_argument("--dir")
	parser.add_argument("--show", default=1, type=int)

	args = parser.parse_args()
	assert args.type in ("train", "test", "test_export_images", "retrain", "test_dataset", "test_export_npz")

	args.resolution = tuple(map(lambda x : int(x), args.resolution.split(",")))
	args.weights_file = fullPath(args.weights_file)
	args.dir = fullPath(args.dir)
	args.dataset_path = fullPath(args.dataset_path)
	args.firstLinksPath = fullPath(args.firstLinksPath)

	# Water the plants
	np.random.seed(args.seed)
	tr.manual_seed(args.seed)
	return args

def main():
	args = getArgs()
	model, dataDims, hyperParameters = getModelDataDimsAndHyperParams(args, globals()[args.graph_config])
	reader = Reader(args.dataset_path, dataDims=dataDims, hyperParameters=hyperParameters)
	generator, numSteps, valGenerator, valNumSteps = getGenerators(reader, args.batch_size, maxPrefetch=1)

	if args.type == "test_dataset":
		changeDirectory(args.dir, expectExist=False)
		test_dataset(generator)
		exit()

	optimizerType = {
		"rmsprop" : optim.RMSprop,
		"adamw" : optim.AdamW
	}[args.optimizer]
	hyperParameters["optimizerType"] = args.optimizer
	if "firstLinksPath" in hyperParameters:
		del hyperParameters["firstLinksPath"]
	model.addHyperParameters(hyperParameters)
	model.setOptimizer(optimizerType, lr=args.lr)
	if args.patience > 0:
		model.setOptimizerScheduler(ReduceLRAndBacktrackOnPlateau(model, "Loss", args.patience, args.factor))
	edgeMetricNames, edgeDirections = [], []
	for edge in model.edges:
		for name, callback in edge.getMetrics().items():
			edgeMetricNames.append((str(edge), name))
			edgeDirections.append(callback.getDirection())
	saveModels = [SaveModels("best", name, direction) for (name, direction) in zip(edgeMetricNames, edgeDirections)]
	model.addCallbacks([SaveHistory("history.txt"), *saveModels, PlotMetrics(edgeMetricNames, edgeDirections)])
	print(model.summary())

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		model.draw("graph")
		model.train_generator(generator, numSteps, args.num_epochs, valGenerator, valNumSteps)
	elif args.type == "retrain":
		model.loadModel(args.weights_file)
		changeDirectory(args.dir, expectExist=True)
		model.train_generator(generator, numSteps, args.num_epochs, valGenerator, valNumSteps)
	elif args.type == "test":
		if not args.weights_file is None:
			model.loadWeights(args.weights_file)
		sys.path.append("../other-scripts")
		# from median_callback import MedianCallback
		# model.addCallbacks([MedianCallback(args.graph_config, reader)])
		# model.addCallbacks([MyCallback(args.graph_config, reader)])
		results = model.test_generator(valGenerator, valNumSteps, printMessage="v2")
		print("\nResults: %s" % str(results))
	elif args.type == "test_export_images":
		model.loadWeights(args.weights_file)
		model.eval()
		changeDirectory(args.dir, expectExist=False)
		test_export_images(model, generator, numSteps, valGenerator, valNumSteps, args.show)
	elif args.type == "test_export_npz":
		model.loadWeights(args.weights_file)
		model.eval()
		changeDirectory(args.dir, expectExist=False)
		test_export_npz(model, valGenerator, valNumSteps)

if __name__ == "__main__":
	main()
