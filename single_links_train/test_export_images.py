import matplotlib.pyplot as plt
import numpy as np
import cv2
from neural_wrappers.pytorch import npGetData
from neural_wrappers.utilities import npCloseEnough

from cycleconcepts.plot_utils import toImageFuncs

def test_export_images(model, generator, numSteps, valGenerator, valNumSteps, show):
	cnt = 1 # Used for savefig
	numNonZero = 0
	for items in generator:
		data, labels = items
		result, loss = model.mainLoop(data, labels)
		print({k : [x.shape for x in result[k]] for k in result.keys()})

		MB = data[list(data.keys())[0]].shape[0]
		for i in range(MB):
			for edge in model.edges:
				edgeID = str(edge)
				A, B = edge.inputNode, edge.outputNode
				GT = npGetData(B.getGroundTruth())
				results = np.array(npGetData(result[edgeID]))
				# if B.name.split(" ")[0] == "Depth":
				# 	GT = GT["computed"]
				# 	if A.name.split(" ")[0] != "RGB":
				# 		results = np.expand_dims(results, axis=0)
				if type(GT) is dict:
					print(GT.keys())
				GT = GT[i]
				GTImage = toImageFuncs[type(B)](GT, GT, B)
				# inputs = npGetData(edge.inputs)
				# print(type(inputs), inputs.keys())
				inputs = npGetData(edge.inputs)["GT"][:, i]
				
				results = results[:, i]

				numInputs = len(inputs)
				print("Edge %s has %d inputs" % (edgeID, numInputs))
				for j in range(numInputs):
					thisInput = inputs[j]
					thisResult = results[j]
					# resMetrics = getMetrics(GT, thisResult, edge.getMetrics())

					arr = plt.subplots(1, 3)[1]
					try:
						thisInputImage = toImageFuncs[type(A)](thisInput, GT, A)
					except Exception as e:
						print(e)
						thisInputImage = np.zeros((256, 256), dtype=np.uint8)

					thisResultImage = toImageFuncs[type(B)](thisResult, GT, B)
					if np.count_nonzero(thisResult > 0.1) > 10:
						numNonZero += 1
						print(f'total non zero:{numNonZero/cnt}')
					
					plt.gcf().set_size_inches(10, 5)
					plt.clf()
					plt.cla()
					plt.axis("off")
					plt.imshow(thisInputImage)
					plt.box(False)
					plt.savefig("%d_edge%s_input_%d_input.png" % (cnt, edgeID, j + 1),  bbox_inches='tight', pad_inches=0)

					plt.clf()
					plt.cla()
					plt.axis("off")
					plt.imshow(thisResultImage)
					plt.box(False)
					plt.savefig("%d_edge%s_input_%d_predict.png" % (cnt, edgeID, j + 1),  bbox_inches='tight', pad_inches=0)

					plt.clf()
					plt.cla()
					plt.axis("off")
					plt.imshow(GTImage)
					plt.box(False)
					plt.savefig("%d_edge%s_input_%d_GT.png" % (cnt, edgeID, j + 1),  bbox_inches='tight', pad_inches=0)

					plt.close()

			cnt += 1
			# if cnt == 20:
			# 	exit()
