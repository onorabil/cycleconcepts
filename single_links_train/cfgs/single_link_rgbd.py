# Extension of single link cfgs to RGBD compound node
from neural_wrappers.graph import Graph, Edge
from ..nodes import RGBD, Pose

def pose2rgbd(args, reader):
	rgbdNode = RGBD(maxDepthMeters=args.max_depth_meters)
	poseNode = Pose(reader.positionsExtremes)
	return Graph([
		Edge(poseNode, rgbdNode)
	])

def rgbd2pose(args, reader):
	rgbdNode = RGBD(maxDepthMeters=args.max_depth_meters)
	poseNode = Pose(reader.positionsExtremes)
	return Graph([
		Edge(rgbdNode, poseNode)
	])