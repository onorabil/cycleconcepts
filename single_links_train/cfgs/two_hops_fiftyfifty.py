from neural_wrappers.graph import forwardUseAll, forwardUseGT, Graph, Edge, ConcatenateNode
from cycleconcepts.nodes import RGB, Depth, Semantic, Halftone, Normal, CameraNormal, OpticalFlow, RGBNeighbour, Wireframe, Pose
from .rgb2opticalflow import concatenateFn, EncoderMap2Map

def rgb2depth2semantic_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semanticNode = Semantic()

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2depthPath = "%s/rgb2depth/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2depth.loadPretrainedEdge(rgb2depthPath, trainable=False)
	depth2semantic = Edge(depthNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2depth,
		depth2semantic
	])

def rgb2halftone2semantic_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	halftoneNode = Halftone()
	semanticNode = Semantic()

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2halftone.loadPretrainedEdge(rgb2halftonePath, trainable=False)
	halftone2semantic = Edge(halftoneNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2halftone,
		halftone2semantic
	])

def rgb2normal2semantic_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	normalNode = Normal()
	semanticNode = Semantic()

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2normal.loadPretrainedEdge(rgb2normalPath, trainable=False)
	normal2semantic = Edge(normalNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2normal,
		normal2semantic
	])

def rgb2wireframe2semantic_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	wireframeNode = Wireframe()
	semanticNode = Semantic()

	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2wireframe.loadPretrainedEdge(rgb2wireframePath, trainable=False)
	wirerame2semantic = Edge(wireframeNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2wireframe,
		wirerame2semantic
	])

def rgb2cameranormal2semantic_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	semanticNode = Semantic()

	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2cameranormal.loadPretrainedEdge(rgb2cameranormalPath, trainable=False)
	cameranormal2semantic = Edge(cameranormalNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2cameranormal,
		cameranormal2semantic
	])

def rgb2opticalflow2semantic_fiftyfifty(hyperParameters):
	# Nodes definition
	rgbNode = RGB()
	rgbNeighbourNode = RGBNeighbour(skip=hyperParameters["opticalFlowSkip"])
	rgbPlusNeighbourNode = ConcatenateNode([rgbNode, rgbNeighbourNode], concatenateFn=concatenateFn, \
		nodeEncoder=EncoderMap2Map(dIn=6))
	opticalFlowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], \
		opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	semanticNode = Semantic()

	rgbCat2OpticalFlow = Edge(rgbPlusNeighbourNode, opticalFlowNode, forwardFn=forwardUseGT, blockGradients=True)
	rgbCat2OpticalFlowPath = "%s/rgb2opticalflow/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgbCat2OpticalFlow.loadPretrainedEdge(rgbCat2OpticalFlowPath, trainable=False)
	opticalFlow2semantic = Edge(opticalFlowNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)

	return Graph([
		rgbCat2OpticalFlow,
		opticalFlow2semantic
	])

def rgb2halftone2depth_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	halftoneNode = Halftone()
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2halftone.loadPretrainedEdge(rgb2halftonePath, trainable=False)
	halftone2depth = Edge(halftoneNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2halftone,
		halftone2depth
	])

def rgb2normal2depth_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	normalNode = Normal()
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2normal.loadPretrainedEdge(rgb2normalPath, trainable=False)
	normal2depth = Edge(normalNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2normal,
		normal2depth
	])

def rgb2cameranormal2depth_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2cameranormal.loadPretrainedEdge(rgb2cameranormalPath, trainable=False)
	cameranormal2depth = Edge(cameranormalNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2cameranormal,
		cameranormal2depth
	])

def rgb2opticalflow2depth_fiftyfifty(hyperParameters):
	# Nodes definition
	rgbNode = RGB()
	rgbNeighbourNode = RGBNeighbour(skip=hyperParameters["opticalFlowSkip"])
	rgbPlusNeighbourNode = ConcatenateNode([rgbNode, rgbNeighbourNode], concatenateFn=concatenateFn, \
		nodeEncoder=EncoderMap2Map(dIn=6))
	opticalFlowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], \
		opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgbCat2OpticalFlow = Edge(rgbPlusNeighbourNode, opticalFlowNode, forwardFn=forwardUseGT, blockGradients=True)
	rgbCat2OpticalFlowPath = "%s/rgb2opticalflow/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgbCat2OpticalFlow.loadPretrainedEdge(rgbCat2OpticalFlowPath, trainable=False)
	opticalFlow2depth = Edge(opticalFlowNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)

	return Graph([
		rgbCat2OpticalFlow,
		opticalFlow2depth
	])

def rgb2semantic2depth_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	semanticNode = Semantic()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2semanticPath = "%s/rgb2semantic/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2semantic.loadPretrainedEdge(rgb2semanticPath, trainable=False)
	semantic2Depth = Edge(semanticNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2semantic,
		semantic2Depth
	])

def rgb2wireframe2depth_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	wireframeNode = Wireframe()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2wireframe.loadPretrainedEdge(rgb2wireframePath, trainable=False)
	wirerame2depth = Edge(wireframeNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2wireframe,
		wirerame2depth
	])

def rgb2pose2semantic_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], \
		poseRepresentation="6dof_quat", outResolution=hyperParameters["resolution"])
	semanticNode = Semantic()

	rgb2pose = Edge(rgbNode, poseNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2posePath = "%s/rgb2pose/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2pose.loadPretrainedEdge(rgb2posePath, trainable=False)
	pose2semantic = Edge(poseNode, semanticNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2pose,
		pose2semantic
	])

def rgb2pose2depth_fiftyfifty(hyperParameters):
	rgbNode = RGB()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], \
		poseRepresentation="6dof_quat", outResolution=hyperParameters["resolution"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2pose = Edge(rgbNode, poseNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2posePath = "%s/rgb2pose/model_best_Loss.pkl" % (hyperParameters["firstLinksPath"])
	rgb2pose.loadPretrainedEdge(rgb2posePath, trainable=False)
	pose2depth = Edge(poseNode, depthNode, forwardFn=forwardUseAll, blockGradients=True)
	return Graph([
		rgb2pose,
		pose2depth
	])