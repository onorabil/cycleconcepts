from neural_wrappers.graph import Graph, Edge
from functools import partial
from cycleconcepts.nodes import RGB, Pose, Depth, Normal, Semantic, Wireframe, CameraNormal, \
	Halftone, OpticalFlow

def rgb2wireframe(hyperParameters):
	rgbNode = RGB()
	wireframeNode = Wireframe()
	return Graph([
		Edge(rgbNode, wireframeNode, blockGradients=False)
	])

def rgb2semantic(hyperParameters):
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(rgbNode, semanticNode, blockGradients=False)
	])

def rgb2normal(hyperParameters):
	rgbNode = RGB()
	normalNode = Normal()
	return Graph([
		Edge(rgbNode, normalNode, blockGradients=False)
	])

def rgb2cameranormal(hyperParameters):
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(rgbNode, cameranormalNode, blockGradients=False)
	])

def rgb2pose(hyperParameters):
	rgbNode = RGB()

	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(rgbNode, poseNode, blockGradients=False)
	])

def rgb2depth(hyperParameters):
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(rgbNode, depthNode, blockGradients=False)
	])

def rgb2halftone(hyperParameters):
	rgbNode = RGB()
	halftoneNode = Halftone()
	return Graph([
		Edge(rgbNode, halftoneNode, blockGradients=False)
	])

def wireframe2rgb(hyperParameters):
	wireframeNode = Wireframe()
	rgbNode = RGB()
	return Graph([
		Edge(wireframeNode, rgbNode, blockGradients=False)
	])

def wireframe2semantic(hyperParameters):
	wireframeNode = Wireframe()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(wireframeNode, semanticNode, blockGradients=False)
	])

def wireframe2normal(hyperParameters):
	wireframeNode = Wireframe()
	normalNode = Normal()
	return Graph([
		Edge(wireframeNode, normalNode, blockGradients=False)
	])

def wireframe2cameranormal(hyperParameters):
	wireframeNode = Wireframe()
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(wireframeNode, cameranormalNode, blockGradients=False)
	])

def wireframe2pose(hyperParameters):
	wireframeNode = Wireframe()
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(wireframeNode, poseNode, blockGradients=False)
	])

def wireframe2depth(hyperParameters):
	wireframeNode = Wireframe()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(wireframeNode, depthNode, blockGradients=False)
	])

def wireframe2halftone(hyperParameters):
	wireframeNode = Wireframe()
	halftoneNode = Halftone()
	return Graph([
		Edge(wireframeNode, halftoneNode, blockGradients=False)
	])

def semantic2rgb(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	rgbNode = RGB()
	return Graph([
		Edge(semanticNode, rgbNode, blockGradients=False)
	])

def semantic2wireframe(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	wireframeNode = Wireframe()
	return Graph([
		Edge(semanticNode, wireframeNode, blockGradients=False)
	])

def semantic2normal(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	normalNode = Normal()
	return Graph([
		Edge(semanticNode, normalNode, blockGradients=False)
	])

def semantic2cameranormal(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(semanticNode, cameranormalNode, blockGradients=False)
	])

def semantic2pose(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(semanticNode, poseNode, blockGradients=False)
	])

def semantic2depth(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(semanticNode, depthNode, blockGradients=False)
	])

def semantic2halftone(hyperParameters):
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	halftoneNode = Halftone()
	return Graph([
		Edge(semanticNode, halftoneNode, blockGradients=False)
	])

def opticalflow2rgb(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	rgbNode = RGB()
	return Graph([
		Edge(opticalflowNode, rgbNode, blockGradients=False)
	])

def opticalflow2wireframe(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	wireframeNode = Wireframe()
	return Graph([
		Edge(opticalflowNode, wireframeNode, blockGradients=False)
	])

def opticalflow2semantic(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(opticalflowNode, semanticNode, blockGradients=False)
	])

def opticalflow2normal(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	normalNode = Normal()
	return Graph([
		Edge(opticalflowNode, normalNode, blockGradients=False)
	])

def opticalflow2cameranormal(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(opticalflowNode, cameranormalNode, blockGradients=False)
	])

def opticalflow2pose(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(opticalflowNode, poseNode, blockGradients=False)
	])

def opticalflow2depth(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(opticalflowNode, depthNode, blockGradients=True)
	])

def opticalflow2halftone(hyperParameters):
	opticalflowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	halftoneNode = Halftone()
	return Graph([
		Edge(opticalflowNode, halftoneNode, blockGradients=False)
	])

def normal2rgb(hyperParameters):
	normalNode = Normal()
	rgbNode = RGB()
	return Graph([
		Edge(normalNode, rgbNode, blockGradients=False)
	])

def normal2wireframe(hyperParameters):
	normalNode = Normal()
	wireframeNode = Wireframe()
	return Graph([
		Edge(normalNode, wireframeNode, blockGradients=False)
	])

def normal2semantic(hyperParameters):
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(normalNode, semanticNode, blockGradients=False)
	])

def normal2cameranormal(hyperParameters):
	normalNode = Normal()
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(normalNode, cameranormalNode, blockGradients=False)
	])

def normal2pose(hyperParameters):
	normalNode = Normal()
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(normalNode, poseNode, blockGradients=False)
	])

def normal2depth(hyperParameters):
	normalNode = Normal()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(normalNode, depthNode, blockGradients=False)
	])

def normal2halftone(hyperParameters):
	normalNode = Normal()
	halftoneNode = Halftone()
	return Graph([
		Edge(normalNode, halftoneNode, blockGradients=False)
	])

def cameranormal2rgb(hyperParameters):
	cameranormalNode = CameraNormal()
	rgbNode = RGB()
	return Graph([
		Edge(cameranormalNode, rgbNode, blockGradients=False)
	])

def cameranormal2wireframe(hyperParameters):
	cameranormalNode = CameraNormal()
	wireframeNode = Wireframe()
	return Graph([
		Edge(cameranormalNode, wireframeNode, blockGradients=False)
	])

def cameranormal2semantic(hyperParameters):
	cameranormalNode = CameraNormal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(cameranormalNode, semanticNode, blockGradients=False)
	])

def cameranormal2normal(hyperParameters):
	cameranormalNode = CameraNormal()
	normalNode = Normal()
	return Graph([
		Edge(cameranormalNode, normalNode, blockGradients=False)
	])

def cameranormal2pose(hyperParameters):
	cameranormalNode = CameraNormal()
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(cameranormalNode, poseNode, blockGradients=False)
	])

def cameranormal2depth(hyperParameters):
	cameranormalNode = CameraNormal()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(cameranormalNode, depthNode, blockGradients=False)
	])

def cameranormal2halftone(hyperParameters):
	cameranormalNode = CameraNormal()
	halftoneNode = Halftone()
	return Graph([
		Edge(cameranormalNode, halftoneNode, blockGradients=False)
	])

def pose2rgb(hyperParameters):
	poseNode = Pose(**hyperParameters)
	rgbNode = RGB()
	return Graph([
		Edge(poseNode, rgbNode, blockGradients=False)
	])

def pose2wireframe(hyperParameters):
	poseNode = Pose(**hyperParameters)
	wireframeNode = Wireframe()
	return Graph([
		Edge(poseNode, wireframeNode, blockGradients=False)
	])

def pose2semantic(hyperParameters):
	poseNode = Pose(**hyperParameters)
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(poseNode, semanticNode, blockGradients=False)
	])

def pose2normal(hyperParameters):
	poseNode = Pose(**hyperParameters)
	normalNode = Normal()
	return Graph([
		Edge(poseNode, normalNode, blockGradients=False)
	])

def pose2cameranormal(hyperParameters):
	poseNode = Pose(**hyperParameters)
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(poseNode, cameranormalNode, blockGradients=False)
	])

def pose2depth(hyperParameters):
	poseNode = Pose(**hyperParameters)
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(poseNode, depthNode, blockGradients=False)
	])

def pose2halftone(hyperParameters):
	poseNode = Pose(**hyperParameters)
	halftoneNode = Halftone()
	return Graph([
		Edge(poseNode, halftoneNode, blockGradients=False)
	])

def depth2rgb(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	rgbNode = RGB()
	return Graph([
		Edge(depthNode, rgbNode, blockGradients=False)
	])

def depth2wireframe(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	wireframeNode = Wireframe()
	return Graph([
		Edge(depthNode, wireframeNode, blockGradients=False)
	])

def depth2semantic(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(depthNode, semanticNode, blockGradients=False)
	])

def depth2normal(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	normalNode = Normal()
	return Graph([
		Edge(depthNode, normalNode, blockGradients=False)
	])

def depth2cameranormal(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(depthNode, cameranormalNode, blockGradients=False)
	])

def depth2pose(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(depthNode, poseNode, blockGradients=False)
	])

def depth2halftone(hyperParameters):
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	halftoneNode = Halftone()
	return Graph([
		Edge(depthNode, halftoneNode, blockGradients=False)
	])

def halftone2rgb(hyperParameters):
	halftoneNode = Halftone()
	rgbNode = RGB()
	return Graph([
		Edge(halftoneNode, rgbNode, blockGradients=False)
	])

def halftone2wireframe(hyperParameters):
	halftoneNode = Halftone()
	wireframeNode = Wireframe()
	return Graph([
		Edge(halftoneNode, wireframeNode, blockGradients=False)
	])

def halftone2semantic(hyperParameters):
	halftoneNode = Halftone()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return Graph([
		Edge(halftoneNode, semanticNode, blockGradients=False)
	])

def halftone2normal(hyperParameters):
	halftoneNode = Halftone()
	normalNode = Normal()
	return Graph([
		Edge(halftoneNode, normalNode, blockGradients=False)
	])

def halftone2cameranormal(hyperParameters):
	halftoneNode = Halftone()
	cameranormalNode = CameraNormal()
	return Graph([
		Edge(halftoneNode, cameranormalNode, blockGradients=False)
	])

def halftone2pose(hyperParameters):
	halftoneNode = Halftone()
	poseNode = Pose(**hyperParameters)
	return Graph([
		Edge(halftoneNode, poseNode, blockGradients=False)
	])

def halftone2depth(hyperParameters):
	halftoneNode = Halftone()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return Graph([
		Edge(halftoneNode, depthNode, blockGradients=False)
	])

