import torch as tr
from neural_wrappers.graph import Node, ConcatenateNode, Graph, Edge, Node, forwardUseGT
from functools import partial

from cycleconcepts.models import EncoderMap2Map
from cycleconcepts.nodes import RGB, Position, Orientation, PoseSplitNode

def f(obj, x):
	x.pop("GT")
	return [{str(obj.inputNode) : tuple(x.values())[0][0]}]

def rgb2posesplit(hyperParameters):
	# Nodes definition
	rgbNode = RGB()
	position = Position(positionExtremes=hyperParameters["positionExtremes"], \
		positionLossType=hyperParameters["positionLossType"])
	orientation = Orientation(orientationLossType=hyperParameters["orientationLossType"])
	poseCommon = PoseSplitNode()

	# Edges definition
	rgb2Position = Edge(rgbNode, position, forwardFn=forwardUseGT, blockGradients=True)
	rgb2Orientation = Edge(rgbNode, orientation, forwardFn=forwardUseGT, blockGradients=True)
	orientation2Common = Edge(orientation, poseCommon, forwardFn=f, blockGradients=True)
	position2Common = Edge(position, poseCommon, forwardFn=f, blockGradients=True)

	graph = Graph([
		rgb2Position,
		rgb2Orientation,
		orientation2Common,
		position2Common
	])

	return graph

def rgb2orientation(hyperParameters):
	# Nodes definition
	rgbNode = RGB()
	orientation = Orientation(orientationLossType=hyperParameters["orientationLossType"])

	# Edges definition
	rgb2Orientation = Edge(rgbNode, orientation, forwardFn=forwardUseGT, blockGradients=True)

	graph = Graph([
		rgb2Orientation,
	])

	return graph

def rgb2position(hyperParameters):
	# Nodes definition
	rgbNode = RGB()
	position = Position(positionExtremes=hyperParameters["positionExtremes"], \
		positionLossType=hyperParameters["positionLossType"])

	# Edges definition
	rgb2Position = Edge(rgbNode, position, forwardFn=forwardUseGT, blockGradients=True)

	graph = Graph([
		rgb2Position,
	])

	return graph
