import torch as tr
from neural_wrappers.graph import Node, ConcatenateNode, Graph, Edge, forwardUseGT, \
	MapNode, forwardUseIntermediateResult
from functools import partial

from cycleconcepts.models import EncoderMap2Map
from cycleconcepts.nodes import OpticalFlow, RGB, Depth, Semantic, RGBNeighbour
from .utils import getSingleLinkPath

# Not accounting for multiple messages here :)
def concatenateFn(x):
	result = list(x.values())
	try:
		result = tr.cat(result, dim=-1)
	except Exception:
		print("\n\n\nError here", [x.shape for x in result])
		exit()
	return result

def rgb2opticalflow(hyperParameters):
	# Nodes definition
	rgbNode = RGB()
	rgbNeighbourNode = RGBNeighbour(skip=hyperParameters["opticalFlowSkip"])
	rgbPlusNeighbourNode = ConcatenateNode([rgbNode, rgbNeighbourNode], concatenateFn=concatenateFn, \
		nodeEncoder=EncoderMap2Map(dIn=6))
	opticalFlowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], \
		opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	rgbCat2OpticalFlow = Edge(rgbPlusNeighbourNode, opticalFlowNode, forwardFn=forwardUseGT, blockGradients=True)

	graph = Graph([
		rgbCat2OpticalFlow
	])

	return graph

def rgb2opticalflow2depth(hyperParameters, rgb2DepthPretrained=False):
	# Nodes definition
	rgbNode = RGB()
	rgbNeighbourNode = RGBNeighbour(skip=hyperParameters["opticalFlowSkip"])
	rgbPlusNeighbourNode = ConcatenateNode([rgbNode, rgbNeighbourNode], concatenateFn=concatenateFn, \
		nodeEncoder=EncoderMap2Map(dIn=6))
	opticalFlowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], \
		opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	paths = {k : "%s/%s/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"], k) for k in
		["rgb2opticalflow", "opticalflow2depth"]
	}

	rgbCat2OpticalFlow = Edge(rgbPlusNeighbourNode, opticalFlowNode, forwardFn=forwardUseGT, blockGradients=True)
	rgbCat2OpticalFlow.loadPretrainedEdge(paths["rgb2opticalflow"], trainable=False)
	opticalFlow2Depth = Edge(opticalFlowNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	opticalFlow2Depth.loadPretrainedEdge(paths["opticalflow2depth"], trainable=False)

	graph = Graph([
		rgbCat2OpticalFlow,
		opticalFlow2Depth
	])

	return graph

def rgb2opticalflow2semantic(hyperParameters, rgb2DepthPretrained=False):
	# Nodes definition
	rgbNode = RGB()
	rgbNeighbourNode = RGBNeighbour(skip=hyperParameters["opticalFlowSkip"])
	rgbPlusNeighbourNode = ConcatenateNode([rgbNode, rgbNeighbourNode], concatenateFn=concatenateFn, \
		nodeEncoder=EncoderMap2Map(dIn=6))
	opticalFlowNode = OpticalFlow(opticalFlowPercentage=hyperParameters["opticalFlowPercentage"], \
		opticalFlowMode=hyperParameters["opticalFlowMode"], opticalFlowSkip=hyperParameters["opticalFlowSkip"])
	semanticNode = Semantic()

	paths = {k : "%s/%s/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"], k) for k in
		["rgb2opticalflow", "opticalflow2semantic"]
	}

	rgbCat2OpticalFlow = Edge(rgbPlusNeighbourNode, opticalFlowNode, forwardFn=forwardUseGT, blockGradients=True)
	rgbCat2OpticalFlow.loadPretrainedEdge(paths["rgb2opticalflow"], trainable=False)
	opticalFlow2Semantic = Edge(opticalFlowNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	opticalFlow2Semantic.loadPretrainedEdge(paths["opticalflow2semantic"], trainable=False)

	graph = Graph([
		rgbCat2OpticalFlow,
		opticalFlow2Semantic
	])

	return graph