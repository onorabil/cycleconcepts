from .single_link import *
# Optical Flow, which is a special kind of single link/two hopper
from .rgb2opticalflow import rgb2opticalflow

from .two_hops_fiftyfifty import *
from .two_hops_nogt import *

# from .rgb2semantic_iter1 import rgb2semantic_iter1, rgb2semantic_iter1_2depth_nogt
# from .rgb2depth_iter1 import rgb2depth_iter1, rgb2depth_iter1_2semantic_nogt