# Single links train

This subrepository is reponsible for training the single links either independently or using a two-hops graph cfg. Anything that uses more than 3 nodes is usually implemented in cfgs in the other subrepository (fine_tune_algorithms).

The basic command for training a cfg follows the following rule:
```
python3 main.py <mode> <cfg> </path/to/dataset.h5> --hyperParameter=value
```

## __Modes__ 
Can be the following:
- train - The main function used to train a model from scratch. Stores checkpoints in a directory specified at `--dir`, which is expected to not exist.
- retrain - Takes a checkpoint (specified by `--weights_file`) and retrains (continues from that checkpoint) for a number of epochs between the number of epochs of the checkpoint and the desired number (specified by `--num_epochs`). Checkpoints are stored at `--dir`, which is expected to exist.
- test - Takes a checkpoint (specified by `--weights_file`) and tests on the `</path/to/dataset.h5>` dataset, resulting in a print of the metrics associated with the `<cfg>`.
- test_export_images - Takes a checkpoint (specified by `--weights_file`) and stores predictions as images in `--dir`.
- test_export_npz - Takes a checkpoint (specified by `--weights_file`) and stores predictions as npy files in `--dir`.

## __Hyperparameters__
Some of them are CFG defined and will raise an error if not specified depending on combinations of nodes or links. However, some of them are standard:
- dir - Specifies the working directory
- weights_file - Specifies a path to a checkpoint. Some CFGs will use other hyperparameters to specify paths to checkpoints, but most of the times this is the main one used.
- batch_size, num_epochs, lr, seed - Self-explanatory, used for training.
- resolution - The resolution used for image resizing. Uses the following format: __h,w__. Defaults to 512x512, however 256x256 are used for latest experiments. So always use `--resolution=256,256`. Parsing is done using:
```
args.resolution = list(map(lambda x : int(x), args.resolution.split(",")))
```

## CFGs
The graph project is made of graph configurations with self-explanatory names (e.g.: `rgb2depth`, `cameranormal2semantic2pose` etc.). These are stored as `globals` and loaded from `cfgs/single_links.py` or similar files.

Note that these files are generated, so adding a new single link should not be done in those files, but rather the generator script should be updates (see `../other-scripts`). Some CFGs have some special hyperParameters that, if not specified, will throw an error. This is implemented in `../cycleconcepts/utils.py`, however it is prone for modifications. Example of such hyperparameters:
- depth node:
    
   `--maxDepthMeters` (300 is used for all experiments, some CFGs may not check training hyperparameters due to various hacks used every now and then which will be updated at some point, so always specifiy it for depth nodes, even if no error is thrown)
- optical flow node in `XXX2opticalflow` edges:

    `--opticalFlowMode=Mode` - Mode can be magnitude, xy or xy_plus_magnitude

    `--opticalFlowPercentage=x%,y%` - Same parsing rule as `--resolution`)

    `--opticalFlowSkip=N` - N is a value representing how many frames ahead the RGB neighbour is taken into account. Only `rgb2opticalflow` is implemented as a separate single link (so, for example `halftone2opticalflow` using two neighbouring halftone images is not supported yet).

## TL;DR
```
python3 main.py train rgb2depth /path/to/train.h5 --resolution=256,256 --maxDepthMeters=300 --dir=test
```
Then, you wait for training to be done (100 epochs default)
```
python3 main.py test rgb2depth /path/to/test.h5 --resolution=256,256 --maxDepthMeters=300 --weights_file=test/model_last.pkl
```
This should spit the test set metrics (absolute meters error and such), including loss, to the terminal.
