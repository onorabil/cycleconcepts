import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
from neural_wrappers.pytorch import npGetData
from neural_wrappers.utilities import npCloseEnough

from cycleconcepts.nodes import RGB, Depth, Pose
from cycleconcepts.plot_utils import toImage, toImageFuncs

def getMetrics(GT, thisResult, metrics):
	# Fake a MB of 1
	GT = np.expand_dims(GT, axis=0)
	thisResult = np.expand_dims(thisResult, axis=0)
	res = {}
	for k in metrics:
		if k == "Loss" or (type(k) == tuple and k[1] == "Loss"):
			continue
		res[k] = metrics[k](GT, thisResult)
	# Stringify the reusult
	res = "\n".join(["%s: %2.3f" % (k, res[k]) for k in res])
	return res

def test_export_video(model, generator, numSteps, valGenerator, valNumSteps, show):
	#model['hyperparameters']
	clip_width = 512*3
	clip_height = 512 
	writer = cv2.VideoWriter('out_clip.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 15, (clip_width, clip_height), True)
	cnt = 1 # Used for savefig
	for idx_item, items in enumerate(generator):
		if idx_item == numSteps:
			break
		print(idx_item)
		if idx_item < 1:
			continue
		data, labels = items
		result, loss = model.mainLoop(data, labels)
		print({k : [x.shape for x in result[k]] for k in result.keys()})

		MB = data[list(data.keys())[0]].shape[0]
		for i in range(MB):
			rgb = data["rgbDomain2"][i]
			for edge in model.edges:
				edgeID = str(edge)

				A, B = edge.inputNode, edge.outputNode
				if type(A) == RGB and type(B) == Depth:
					pass
				else:
					continue
				GT = npGetData(B.getGroundTruth())[i]
				GTImage = toImageFuncs[type(B)](GT, GT, B)

				GT_width, GT_height, _ = GTImage.shape

				inputs = np.array(npGetData(edge.inputs))[:, i]
				results = np.array(npGetData(result[edgeID]))[:, i]
				numInputs = len(inputs)
				print("Edge %s has %d inputs" % (edgeID, numInputs))
				for j in range(numInputs):
					thisInput = inputs[j]
					thisResult = results[j]
					resMetrics = getMetrics(GT, thisResult, edge.getMetrics())

					thisInputImage = toImageFuncs[type(A)](thisInput, GT, A)

					thisResultImage = toImageFuncs[type(B)](thisResult, GT, B)

					current_frame = np.hstack((thisInputImage[:,:,0:3]*255, thisResultImage[:,:,0:3]*255, GTImage[:,:,0:3]*255))

					current_frame = np.array(current_frame, dtype=np.uint8)

					# RGB >> BGR
					current_frame = current_frame[:,:,::-1]

					#cv2.imwrite('out_clip_frame.png', current_frame)

					writer.write(current_frame)
			
			cnt += 1
			if cnt == 300:
				exit()
			
