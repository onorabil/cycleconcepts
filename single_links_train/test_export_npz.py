import matplotlib.pyplot as plt
import numpy as np
from neural_wrappers.pytorch import npGetData
from tqdm import tqdm

def test_export_npz(model, generator, numSteps):
	cnt = 0 # Used for savefig
	for i in tqdm(range(numSteps)):
		data, labels = next(generator)
		result, loss = model.mainLoop(data, labels)
		Key = str(model.edges[-1])

		result = result[Key][0]
		label = labels["semantic_segmentation"]

		MB = len(result)
		for i in range(MB):
			np.savez_compressed("%d.npz" % cnt, result[i])
			np.savez_compressed("%d_gt.npz" % cnt, label[i])
			#np.save("%d.npy" % cnt, result[i])
			cnt += 1
